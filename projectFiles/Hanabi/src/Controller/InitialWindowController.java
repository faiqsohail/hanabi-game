package Controller;

import View.View;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class InitialWindowController implements Initializable {

    @FXML
    private Button joinButton;

    @FXML
    private Button createButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Stage stage = new Stage();
        stage.setResizable(false);

        joinButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                    Node node = (Node) event.getSource();

                    Object controller = Controller.getController("JoinGameController");
                    Parent parent = View.loadXML("JoinGame", controller);

                    stage.setScene(new Scene(parent));
                    stage.setTitle("Join Game");
                    stage.show();

                    node.getScene().getWindow().hide(); // close the previous window
            }
        });

        createButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                    Node node = (Node) event.getSource();

                    Object controller = Controller.getController("CreateGameController");
                    Parent parent = View.loadXML("CreateGame", controller);

                    stage.setScene(new Scene(parent));
                    stage.setTitle("Create Game");
                    stage.show();

                    node.getScene().getWindow().hide(); // close the previous window
            }
        });
    }
}
