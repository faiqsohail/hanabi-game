package Model;

public class FuseToken {
    private final int MAX = 3;
    private int numberOfFuses;

    /**
     * Constructor for the HintToken class which initially sets the number
     * of hints to the maximum hint number
     */
    FuseToken () {
        this.numberOfFuses = MAX;
    }


    /**
     * Recovers fuse token to the maximum
     */
    public void recoverFuseToken () {
        this.numberOfFuses = MAX;
    }

    public void decreaseFuseToken () {
        if (this.numberOfFuses > 0) {
            this.numberOfFuses -= 1;
        }
    }

    /**
     * access the current number of fuses
     * @return the number of fusetokens as an int
     */
    public int getFuseToken () {
        return this.numberOfFuses;
    }

    public static void main (String [] args) {

        FuseToken testToken = new FuseToken();

        if (testToken.getFuseToken() != 3) {
            System.out.println("Fail to initialize the fuse token number");
        }

        testToken.decreaseFuseToken();
        if (testToken.getFuseToken() != 2) {
            System.out.println("The fuse token does not deacrese to 3 when it was at 3");
        }

        testToken.decreaseFuseToken();
        if (testToken.getFuseToken() != 1) {
            System.out.println("The fuse token does not deacrese to 1 when it was at 2");
        }

        testToken.decreaseFuseToken();
        if (testToken.getFuseToken() != 0) {
            System.out.println("The fuse token does not deacrese to 0 when it was at 1");
        }

        testToken.decreaseFuseToken();
        if (testToken.getFuseToken() < 0) {
            System.out.println("The fuse token exceeds the minimum");
        }

        testToken.recoverFuseToken();
        if (testToken.getFuseToken() != 3) {
            System.out.println("Recover fuse token does not work as intended");
        }

        System.out.println("FuseToken Test Completed");
    }
}
