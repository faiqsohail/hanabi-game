package Model;

import java.util.*;

public class Model {

    /* TODO
    WE should add a your position in here, that way you can reference yourself
    in the player array list, you need to known your own position in the Controller
    class based on player game size
     */


    private String NSID;
    //private View view;
    private Game game;
    private ArrayList <Hint> hints;
    private ArrayList <Card> discardPile;
    private HashMap <Character, Integer> playCardPiles;
    private HashMap<Character, ArrayList<Integer>> wildRainbowPlays;
    private String rainbowMode;
    private int drawPileSize;
    private ArrayList <Player> players;
    private HintToken hintToken;
    private FuseToken fuseToken;
    private String secretKey;
    private String rainbow;
    private int myPosition;


    /**
     * Create model class without any parameters
     */
    public Model () {
        this.hints = new ArrayList<Hint>();

        this.playCardPiles = new HashMap<Character, Integer>();
        this.playCardPiles.put('r',0);
        this.playCardPiles.put('b',0);
        this.playCardPiles.put('w',0);
        this.playCardPiles.put('g',0);
        this.playCardPiles.put('y',0);

        this.wildRainbowPlays = new HashMap<Character, ArrayList<Integer>>();

        this.discardPile = new ArrayList<Card>();
        this.players = new ArrayList<Player>();

        this.hintToken = new HintToken();
        this.fuseToken = new FuseToken();

    }

    /**
     * Constructor to initialize the model class
     * @param numberOfPlayers the number of players for the game class
     * @param TimeOutPeriod the time-out period for the game class
     * @param GameID the ID for the game class
     * @param token the game token for the game class
     * @param nsid the nsid of the game
     * @param rainbowMode indicates if the game has a rainbow mode
     * @param handSize the size of the hand
     */
    public Model (int numberOfPlayers, int TimeOutPeriod, String GameID, String token, String nsid,
                  String rainbowMode, int handSize) {

        this();
        this.game = new Game(numberOfPlayers, TimeOutPeriod, GameID, token);
        setRainbowMode(rainbowMode);
        this.NSID = nsid;

    }

    /**
     * Sets the position of the client player
     * @param myPosition the position of the client player
     */
    public void setMyPosition (int myPosition) {
        this.myPosition = myPosition;
    }

    /**
     * Gets the position of the client player
     * @return position of the client player as an integer
     */
    public int getMyPosition () {
        return this.myPosition;
    }

    public void setRainbowMode (String rainbowMode) {
        this.rainbowMode = rainbowMode;
        if (!rainbowMode.equals("None")) {
            if (rainbowMode.equals("firework")) {
                this.playCardPiles.put('m', 0);
            } else {
                this.drawPileSize = 60 - this.game.getNumberOfPlayers() * this.getCardsPerPlayer();
            }
        } else {
            this.drawPileSize = 50 - this.game.getNumberOfPlayers() * this.getCardsPerPlayer();
        }
    }

    /**
     * obtain the number of cards that are to be put into each player's hands
     * @return the number of cards as an integer
     */
    public int getCardsPerPlayer () {
        int numPlayers = game.getNumberOfPlayers();
        return getCardsPerPlayer(numPlayers);
    }

    /**
     * obtain the number of cards that are to be put into each player's hands
     * @param numPlayers  number of players that are to be in the game
     * @return the number of cards as an integer
     */
    public int getCardsPerPlayer (int numPlayers) {
        if (numPlayers == 2 || numPlayers == 3) {
            return 5;
        } else if (numPlayers == 4 || numPlayers == 5) {
            return 4;
        } else {
            throw new RuntimeException("Should only have game size from 2 - 5");
        }
    }

    /**
     * This method adds player into the model
     * @param player player object to be added to the game
     */
    public void addPlayer (Player player) {
        this.players.add(player);
    }

    /**
     * access the SecretKey of a player
     * @return the serect key of a player as a stirng
     */
    public String getSecretKey () {
        return secretKey;
    }

    /**
     * Set the secretKey of a player
     * @param secretKey the secret key of a player as a string
     */
    public void setSecretKey (String secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * Set the NSID for the model
     * @param nsid the nsid to be put into the NSID field
     */
    public void setNSID (String nsid) {
        this.NSID = nsid;
    }

    /**
     * Obtain the nsid
     * @return the nsid as a String
     */
    public String getNSID () {
        return this.NSID;
    }

    /**
     * Method attempts to fetch the NSID from the operator system environment variable
     * @return String : A validated NSID, null if no valid NSID is found
     */
    public String fetchNSID () {
        String os = System.getProperty("os.name");
        Map<String, String> env = System.getenv();
        String target = null; // target environment variable
        if(os.contains("Linux") || os.contains("Mac")) {
            target = "USER";
        } else if(os.contains("Windows")) {
            target = "USERNAME";
        }
        String nsid = env.get(target);
        return isValidNSID(nsid) ? nsid : null;
    }

    /**
     * Check if the nsid matches the one currently stored
     * @param nsid String nsid to be checked
     * @return true or false as a boolean
     */
    public Boolean checkNSID (String nsid){
        return this.NSID == nsid;
    }

    /**
     * Check if the nsid is a valid nsid format,
     * three alpha followed by three digits. ex. abc123
     * @param nsid String nsid to be validated
     * @return true or false
     */
    public boolean isValidNSID (String nsid) {
        if(nsid != null) {
            nsid = nsid.toLowerCase();
            return nsid.matches("^[a-z]{3}[0-9]{3}$");
        }
        return false;
    }

    /**
     * get the play card pile
     * @return the play card pile as a HashMap
     */
    public HashMap<Character, Integer> getPlayCardPile () {
        return this.playCardPiles;
    }

    /**
     * set the game object
     * @param game the game object
     */
    public void setGame (Game game) {
        this.game = game;
    }


    /**
     * Access the game object in the model
     * @return the game as a game object
     */
    public Game getGame () {
        return this.game;
    }

    /**
     * Add one more hint if the hint is valid
     * @param hint the given hint as a string
     */
    public void addHint (Hint hint) {
        this.hints.add(hint);
    }

    /**
     * get the hint
     * @return the hint as an ArrayList
     */
    public ArrayList<Hint> getHint () {
        return this.hints;
    }

    /**
     * add one more card to the discard pile
     * @param card the given card as a card object
     */
    public void addDiscardPile (Card card){
        this.discardPile.add(card);
    }

    /**
     * Get the whole discard pile
     * @return the most recent discarded card
     */
    public ArrayList<Card> getDiscardPile () {
        return discardPile;
    }

    /**
     * Get the total score
     * @return the total score of tha game
     */
    public int getTotalScore () {
        int total = this.playCardPiles.values().stream().mapToInt(i->i).sum();
        return total;
    }

    /**
     * Get the score for a specific color
     * @param color has to be "r" "g" "y" "w" "b" and "m" if in rainbowMode
     * @return s the score for that color
     */
    public int getScoreByColor (char color) {
        return this.playCardPiles.get(color);
    }

    /**
     * push a new suit number to a exsiting card pile
     * @param card the card
     */
    public void pushPlayPiles (Card card) {
        this.playCardPiles.put(card.getSuit(), card.getRank());
    }

    /**
     * Push a wild rainbow card onto the specified play pile
     * @param card The rainbow card
     * @param pileSuit The suit of the pile the rainbow card is being played on
     */
    public void pushWildRainbowCard(Card card, Character pileSuit){
        if (!this.rainbowMode.equals("wild")){
            throw new RuntimeException("Error, tried to play a wild rainbow card when mode is not wild");
        }
        this.playCardPiles.put(pileSuit, card.getRank());
        this.wildRainbowPlays.putIfAbsent(pileSuit, new ArrayList<Integer>());
        this.wildRainbowPlays.get(pileSuit).add(card.getRank());
    }

    /**
     * Get the wild rainbow plays that have been made
     * @return The hasmap of wild rainbow plays
     */
    public HashMap<Character, ArrayList<Integer>> getWildRainbowPlays(){
        return this.wildRainbowPlays;
    }

    /**
     * Get the rainbow mode
     * @return The rainbow mode
     */
    public String getRainbowMode(){
        return this.rainbowMode;
    }

    /**
     * Draw a card from the play card pile according to the given index
     * size of the draw pile decremt by 1
     */
    public void drawFromPile () {
        if (this.drawPileSize > 0) {
            this.drawPileSize -= 1;
        }
    }

    /**
     * Setter method for the drawPile
     * @param drawPileSize the drawPileSize as an int
     */
    public void setDrawPile (int drawPileSize) {
        this.drawPileSize = drawPileSize;
    }

    /**
     * Getter method for the draw pile
     * @return the draw pile size as an int
     */
    public int getDrawPile () {
        return this.drawPileSize;
    }


    /**
     * Give the current player a card
     * @param position of the player's hand
     * @param card the card to be put into the player's hand
     */
    public void givePlayerCard (int position, Card card) {
        int currentTurn = this.getGame().getPlayerTurn();
        Player currentPlayer = this.players.get(currentTurn);

        currentPlayer.receiveCard(position, card);
    }

    /**
     * Obtain the players
     * @return the players as an ArrayList
     */
    public ArrayList <Player> getPlayers() {
        return this.players;
    }

    /**
     * Set the hint token object
     * @param hintToken the hintoken object to be assigned to the hintToken attribute
     */
    public void setHintToken (HintToken hintToken) {
        this.hintToken = hintToken;
    }

    /**
     * access the hint token attribute
     * @return the hint token attribute as a hint token object
     */
    public HintToken getHintToken () {
        return this.hintToken;
    }

    /**
     * Set the fuse token object
     * @param fuseToken the fusetoken object to be held by the model class
     */
    public void setFuseToken (FuseToken fuseToken) {
        this.fuseToken = fuseToken;
    }

    /**
     * access the fuse token object
     * @return the fuse token object
     */
    public FuseToken getFuseToken () {
        return this.fuseToken;
    }

    /**
     * Obtain the rainbow mode field of the model
     * @return the Rainbow mode as a string
     */
    public String getRainbow() {
        return rainbow;
    }

    /**
     * Set the rainbow mode
     * @param rainbow the rainbow mode string
     */
    public void setRainbow(String rainbow) {
        this.rainbow = rainbow;
    }

    static public void main (String [] args) {

        String[][] cards = {{"g1", "g2", "g3", "g4"},
                {"r1", "r2", "r3", "r4"},
                {"b1", "b2", "b3", "b4"},
                {"y1", "y2", "y3", "y4"},
                {"w1", "w2", "w3", "w4"}};

        Model testModel = new Model(5,
                30,"testGameID",
                "testToken", "abc123",
                "None", 4);

        //test addPlayer
        for (int i = 0; i < 5; i++) {
            Player testPlayer = new Player(new Hand(cards[i], 4), 1);
            testModel.addPlayer(testPlayer);

            if (testModel.getPlayers().get(i) != testPlayer) {
                System.out.println("Error in addPlayer");
            }
        }

        //test getPlayer
        if (testModel.getPlayers() != testModel.players) {
            System.out.println("Error in get players");
        }
        //test the players hands
        if (!testModel.getPlayers().get(0).getHand().toString().equals("g1, g2, g3, g4")) {
            System.out.println("Error initializing player1's hand");
        }
        if (!testModel.getPlayers().get(1).getHand().toString().equals("r1, r2, r3, r4")) {
            System.out.println("Error initializing player2's hand");
        }
        if (!testModel.getPlayers().get(2).getHand().toString().equals("b1, b2, b3, b4")) {
            System.out.println("Error initializing player3's hand");
        }
        if (!testModel.getPlayers().get(3).getHand().toString().equals("y1, y2, y3, y4")) {
            System.out.println("Error initializing player4's hand");
        }
        if (!testModel.getPlayers().get(4).getHand().toString().equals("w1, w2, w3, w4")) {
            System.out.println("Error initializing player5's hand");
        }

        //test getHintToken
        if (testModel.getHintToken() != testModel.hintToken) {
            System.out.println("Error in get HintToken");
        }
        //test getHintToken for decreased hint token
        testModel.getHintToken().decreaseHintToken();
        if (testModel.getHintToken().getHintToken() != 7){
            System.out.println("Error with the HintToken object in Model");
        }
        //test setHintToken
        testModel.setHintToken(new HintToken());
        if (testModel.getHintToken().getHintToken() != 8) {
            System.out.println("Error with set hintToken");
        }
        //test getFuseToken
        if (testModel.getFuseToken() != testModel.fuseToken) {
            System.out.println("Error in get HintToken");
        }
        //test getFuseToken for decreased fuse token
        testModel.getFuseToken().decreaseFuseToken();
        if (testModel.getFuseToken().getFuseToken() != 2){
            System.out.println("Error with the FuseToken object in Model");
        }
        //test setFuseToken
        testModel.setFuseToken(new FuseToken());
        if (testModel.getFuseToken().getFuseToken() != 3) {
            System.out.println("Error with set fuseToken");
        }
        //test getNSID
        if (!testModel.getNSID().equals("abc123")) {
            System.out.println("getNSID error");
        }
        //test setNSID
        testModel.setNSID("def456");
        if (!testModel.getNSID().equals("def456")) {
            System.out.println("setNSID error");
        }
        //test checkNSID
        if (!testModel.checkNSID("def456")) {
           System.out.println("checkNSID error");
        }
        //test getGame
        if(testModel.getGame() != testModel.game){
            System.out.println("Error in getGame");
        }
        //test addHint
        Hint hint = new Hint('r',1);
        testModel.addHint(hint);
        if(testModel.hints.get(0) != hint){
            System.out.println("Error in add hint");
        }
        //test getHint
        if(testModel.getHint() != testModel.hints){
            System.out.println("Error in get hint");
        }

        Card r1 = new Card(1,'r');
        Card r2 = new Card(2,'r');
        Card g1 = new Card(1,'g');
        Card y1 = new Card(1,'y');
        Card b1 = new Card(1,'b');

        //test addDiscard pile
        testModel.addDiscardPile(r1);
        testModel.addDiscardPile(g1);
        if (testModel.getDiscardPile() != testModel.discardPile) {
            System.out.println("Error with the getDiscardPile");
        }
        if (testModel.getDiscardPile().get(0).getSuit() != 'r') {
            System.out.println("Error in getDiscardPile or addDiscardPile");
        }
        if (testModel.getDiscardPile().get(1).getSuit() != 'g') {
            System.out.println("Error in getDiscardPile or addDiscardPile");
        }

        //test getDrawPile
        if (testModel.getDrawPile() != (50 - 5 * 4)) {
            System.out.println(testModel.getGame().getNumberOfPlayers());
            System.out.println("Error in getDrawPile");
        }
        //test setDrawPile
        testModel.setDrawPile(20);
        if (testModel.getDrawPile() != 20) {
            System.out.println("Error in setDrawPile");
        }
        //test drawfromPile
        testModel.drawFromPile();
        if (testModel.getDrawPile() != 19) {
            System.out.println("Error in drawFromPile");
        }
        testModel.setDrawPile(0);
        testModel.drawFromPile();
        if (testModel.getDrawPile() != 0) {
            System.out.println("Error in drawFromPile");
        }

        //test pushPlayPiles visually
        testModel.pushPlayPiles(r1);
        testModel.pushPlayPiles(r2);
        testModel.pushPlayPiles(b1);
        if(!testModel.playCardPiles.get('r').toString().equals("2")){
            System.out.println("Error in pushPlayPiles");
        }
        if(!testModel.playCardPiles.get('b').toString().equals("1")){
            System.out.println("Error in pushPlayPiles");
        }

        //test GetScorebyColor
        if(testModel.getScoreByColor('r')!=2){
            System.out.println("Error in getScoreByColor");
        }

        //test getTotalScore
        if(testModel.getTotalScore() != 3){
            System.out.println("Error in getTotalScore");
        }

        //test getPlayCardPile
        if (testModel.getPlayCardPile() != testModel.playCardPiles) {
            System.out.println("Error in getPlayCardPile");
        }


        //Test pushWildRainbowCard
        testModel = new Model();
        testModel.setGame(new Game(2, 10, "blah", "blah"));
        testModel.setRainbowMode("wild");
        testModel.pushWildRainbowCard(new Card("m3"), 'b');
        if (testModel.getScoreByColor('b') != 3){
            System.out.println("Error with pushWildRainbowCard, did not properly set play pile for suit");
        }
        if (!testModel.wildRainbowPlays.get('b').contains(3)){
            System.out.println("Error with pusWildRainbowCard, did not properly update wildRainbowPlays");
        }

        testModel.pushWildRainbowCard(new Card("m4"), 'b');
        if (testModel.getScoreByColor('b') != 4){
            System.out.println("Error with pushWildRainbowCard, did not properly set play pile for suit");
        }
        if (!testModel.wildRainbowPlays.get('b').contains(4)){
            System.out.println("Error with pusWildRainbowCard, did not properly update wildRainbowPlays");
        }

        //test setMyPosition() and getMyPosition()
        testModel.setMyPosition(2);
        if (testModel.getMyPosition() != 2) {
            System.out.println("Error with getMyPositioon and setMyPostion");
        }
    }

}
