package Controller;
import Model.Model;
import Model.Card;
import Network.Action;
import Network.Message;
import Startup.Client;
import View.PlayerCardView;
import View.View;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.json.simple.JSONArray;

import java.net.URL;
import java.util.*;


public class GameController extends Controller implements Initializable {

    @FXML
    private BorderPane borderPane;

    @FXML
    private Label fuse;

    @FXML
    private Label information;

    @FXML
    private TitledPane yourHand;

    private Thread thread;
    protected Model model;

    public static String moveSelection;

    protected static HashMap<Character, String> suitMap = new HashMap<>();
    protected static HashMap<Integer, Integer> positionMap = new HashMap<>();
    

    public GameController (Model model) {
        super(model);
        this.model = model;
    }


    @Override
    public void initialize (URL url, ResourceBundle resourceBundle) {

        suitMap.put('b',"Blue");
        suitMap.put('g',"Green");
        suitMap.put('r',"Red");
        suitMap.put('w', "White");
        suitMap.put('y',"Yellow");
        suitMap.put('m',"Rainbow");

        JSONArray initialHand = model.getGame().getInit_hand();
        String[][] hands = parseJSONArray(initialHand);
        generatePositionMap(hands);

        /*
         * Populate the display with the hands received from the server
         */
        for(int i = 0; i < hands.length; i ++) {
            if(hands[i][0] != null) {
                TitledPane pane = getPlayerPane(positionMap.get(i+1));
                ObservableList items = ((Pane)pane.getContent()).getChildren();
                items.clear(); // clear currently drawn place holder cards in the FXML

                ArrayList<PlayerCardView> cards = generateHand(hands[i], 1);
                for(PlayerCardView card : cards) {
                    items.add(card);
                }
            }
        }

        /*
         * Setup the appropriate listener for when the current user clicks on their own cards.
         */
        yourHand.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(moveSelection != null) {
                    if (event.getPickResult().getIntersectedNode() instanceof ImageView) {
                        ImageView selected = (ImageView) event.getPickResult().getIntersectedNode();
                        ObservableList list = ((Pane) yourHand.getContent()).getChildren();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equals(selected)) {
                                if(moveSelection.equals("playCard")) {
                                    GameController.super.playCard(getPosition(yourHand), i);
                                    Action action = new Action("play", i+1);
                                    action.send(Client.getNetwork().getOutStream());
                                } else if(moveSelection.equals("discardCard")) {
                                    GameController.super.burnCard(getPosition(yourHand), i);
                                    Action action = new Action("discard", i+1);
                                    action.send(Client.getNetwork().getOutStream());
                                }
                                moveSelection = null;
                            }
                        }
                    }
                }
            }
        });

        /*
         * Setup the appropriate listener for when the current user clicks on cards of other players.
         */
        for(TitledPane pane : getAllPlayerHands(borderPane)) {
            if(pane.getId() == null) { // only get TitlePanes for other players, current players TitlePane has an ID.
                pane.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(moveSelection != null) {
                            if (event.getPickResult().getIntersectedNode() instanceof PlayerCardView) {
                                PlayerCardView selected = (PlayerCardView) event.getPickResult().getIntersectedNode();
                                ObservableList list = ((Pane) pane.getContent()).getChildren();
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).equals(selected)) {
                                        if(moveSelection.equals("hintSuit")) {
                                            GameController.super.giveHint(getPosition(pane), selected.getCard().getSuit());
                                            Action action = new Action("inform"
                                                    ,positionMap.get(getPosition(pane)),
                                                    selected.getCard().getSuit());
                                            action.send(Client.getNetwork().getOutStream());
                                        } else if(moveSelection.equals("hintRank")) {
                                            GameController.super.giveHint(getPosition(pane), selected.getCard().getRank());
                                            Action action = new Action("inform"
                                                    ,positionMap.get(getPosition(pane)),
                                                    selected.getCard().getRank());
                                            action.send(Client.getNetwork().getOutStream());
                                        }
                                        moveSelection = null;
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }

        thread = new Thread(new GameControllerUpdater());
        thread.start();
    }

    /**
     * Method that creates an ArrayList of ImagesView's based on the given hand
     * The size of the ImageViews are based on whose hand the method is called for.
     *
     * @param hand - String Array - Player hand input
     * @param handType - int : 0: the client player
     *                         1: other players
     *                         2: the firework pile (middle)
     * @return ArrayList - of ImageViews
     */
    public static ArrayList<PlayerCardView> generateHand (String [] hand, int handType) {
        ArrayList<PlayerCardView> imageViews = new ArrayList<>();

        for (String card : hand){
            if(card != null) {
                char[] chars = card.toCharArray();
                char suit = chars[0];
                char rank = chars[1];

                String suitMapped = suitMap.get(suit);
                String rankImageFile = rank + ".png";

                Image cardImage = new Image(
                        GameController.class.getResourceAsStream("/Assets/" + suitMapped + "/" + rankImageFile));

                PlayerCardView playerCardView = new PlayerCardView(cardImage,
                        new Card(Character.getNumericValue(rank), suit));

                /* Determine the size of a card based on whose hand this is being generated for */
                switch (handType) {
                    case 0:
                        playerCardView.setFitWidth(200.0);
                        playerCardView.setFitHeight(150.0);
                        break;
                    case 1:
                        playerCardView.setFitWidth(80.0);
                        playerCardView.setFitHeight(120.0);
                        break;
                    case 2:
                        playerCardView.setFitWidth(250.0);
                        playerCardView.setFitHeight(200.0);
                        break;
                }

                playerCardView.setPreserveRatio(true);
                playerCardView.setPickOnBounds(true);
                imageViews.add(playerCardView);
            }
        }
        return imageViews;
    }

    /**
     * Method that gets an ArrayList of all the TitlePanes in the current view,
     * as each TitlePane is associated with a Player Hand.
     * @param parent - Parent : The root for which children contain TitlePanes
     * @return ArrayList - of TitlePanes
     */
    public ArrayList<TitledPane> getAllPlayerHands (Parent parent) {
        ArrayList<TitledPane> titledPanes = new ArrayList<>();

        for (Node node : parent.getChildrenUnmodifiable()) {
            if (node instanceof TitledPane) {
                titledPanes.add((TitledPane) node);
            } else if( node instanceof Parent) {
                titledPanes.addAll(getAllPlayerHands((Parent)node));
            }
        }

        return titledPanes;
    }

    /**
     * Returns the position of the player, based on the title of the TitledPane
     * @param pane - TitlePane : The pain to parse
     * @return int - The position of the player on the given pane
     */
    private int getPosition (TitledPane pane) {
        String title = pane.getText();
        return Character.getNumericValue(title.charAt(title.length() - 1));
    }

    /**
     * Returns the TitlePane based on the position of the player
     * @param playerPosition - int : the position player
     * @return TitlePane
     */
    private TitledPane getPlayerPane (int playerPosition) {
        for(TitledPane pane : getAllPlayerHands(borderPane)) {
            if(getPosition(pane) == playerPosition) {
                return pane;
            }
        }
        return null;
    }

    /**
     * Generates the position map given the hand from the server
     * @param hands the hands
     */
    protected static void generatePositionMap (String[][] hands) {
        int totalPlayers = hands.length;
        int myPosition = ((totalPlayers == 2) || (totalPlayers == 3)) ? totalPlayers : totalPlayers - 1;
        ArrayList<Integer> otherPositions = new ArrayList<>();

        for (int i = 0; i < totalPlayers; i ++) {
            int properPosition = i+1;
            if(properPosition != myPosition) {
                otherPositions.add(properPosition);
            }
            if(hands[i][0] == null) {
                positionMap.put(properPosition, myPosition);
            } else {
                positionMap.put(properPosition, null);
            }
        }
        int tmp = 0;
        for(int i = 0; i < otherPositions.size(); i ++) {
            if(positionMap.get(i+1) == null) {
                positionMap.put(i+1, otherPositions.get(i));
            } else {
                tmp = (otherPositions.get(i));
            }
        }
        positionMap.put(totalPlayers, tmp);

    }

    /**
     * Parses a json array to return a 2D java String array
     * @param array JSONArray
     * @return String[][]
     */
    private String[][] parseJSONArray (JSONArray array) {
        String[][] hands = new String[array.size()][model.getCardsPerPlayer()];

        for (int r = 0; r < array.size(); r ++) {
            JSONArray hand = (JSONArray)array.get(r);
            for (int c = 0; c < hand.size(); c++) {
                hands[r][c] = hand.get(c).toString();
            }
        }
        return hands;
    }

    private class GameControllerUpdater implements Runnable {

        @Override
        public void run() {
            while(true) {
                Message message = new Message();
                message.recv(Client.getNetwork().getInStream());
                System.out.println(message.toString());

                //your Move
                if (message.getPayload().containsKey("reply")) {
                    if(message.getPayload().get("reply").equals("your move")) {
                        Platform.runLater(() -> {
                            Stage stage = new Stage();

                            Object controller = Controller.getController("MoveSelectionController");
                            Parent parent = View.loadXML("MoveSelection", controller);
                            stage.setScene(new Scene(parent));
                            stage.setTitle("Move Selection");
                            stage.setAlwaysOnTop(true);
                            stage.setResizable(false);
                            stage.centerOnScreen();
                            stage.show();

                        });
                    }

                    //burned reply
                    else if(message.getPayload().get("reply").equals("burned")) {
                        model.getFuseToken().decreaseFuseToken();
                        if((Boolean)message.getPayload().get("replaced")) {
                            // TODO end the game when the fusetoken is 0;

                            String card = message.getPayload().get("card").toString();
                            Card cardObj = new Card(card);
                            model.addDiscardPile(cardObj); // Add the discarded card to the discarded pile
                            model.getFuseToken().decreaseFuseToken();
                            int remainFuseToken = model.getFuseToken().getFuseToken();
                        }
                    }

                    //built on the stack
                    else if (message.getPayload().get("reply").equals("built")) {
                        if (message.getPayload().get("replaced").equals("true")) {
                            String card = message.getPayload().get("card").toString();
                            Card cardObj = new Card(card);
                            model.pushPlayPiles(cardObj);
                        }
                    }

                    //discarded and inform
                    else if (message.getPayload().get("reply").equals("accepted")) {

                        if (message.getPayload().get("replaced").equals("true")) {
                            String card = message.getPayload().get("card").toString();
                            Card cardObj = new Card(card);
                            model.addDiscardPile(cardObj);
                        }

                        //if none of the above keys exist then it was a confirmation that an inform action went through
                    } else if(message.getPayload().get("reply").equals("game cancelled")) {
                        Platform.runLater(() -> {
                            View.showAlert(Alert.AlertType.INFORMATION, "Game Cancelled",
                                    "The game has been cancelled!");

                            borderPane.getScene().getWindow().hide();
                        });
                        break;
                    } else if (message.getPayload().get("reply").equals("game ends")) {
                        Platform.runLater(() -> {
                            View.showAlert(Alert.AlertType.INFORMATION, "Game Ends",
                                    "Total Score: " + model.getTotalScore());
                            borderPane.getScene().getWindow().hide();
                        });
                        break;
                    }


                }
                if (message.getPayload().containsKey("notice")) {

                    //discard
                    if (message.getPayload().get("notice").equals("discarded")){
                        //ToDo use keys "position" and "drew" to update the model
                        model.setDrawPile(model.getDrawPile() - 1);
                        model.getGame().progressPlayerTurn(); // player turn is progressed after an action from
                    }

                    //inform
                    else if (message.getPayload().get("notice").equals("inform")){
                        //ToDo use keys "player" and "rank"/"suit" to update the model
                        if (message.getPayload().containsKey("rank")) {
                            int rank = (int) message.getPayload().get("rank");
                        }
                        else if (message.getPayload().containsKey("suit")) {
                            String suit = (String) message.getPayload().get("suit");
                        }
                    }

                    //played this is a notice and has been checked
                    else if (message.getPayload().get("notice").equals("played")){
                        if (message.getPayload().containsKey("cards")) {
                            //ToDO update the view and model of our hands with the array of booleans the server gave us "Cards"

                        }
                        //ToDo use keys "position" and "drew" to update the model if we are not the inform recipient
                    }


                }


                Platform.runLater(() -> {
                    fuse.setText("x " + model.getFuseToken().getFuseToken());
                    information.setText("x " + model.getHintToken().getHintToken());
                });
            }
            thread.stop();
        }
    }
}
