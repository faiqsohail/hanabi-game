package Controller;

import Model.Model;
import Model.Player;
import Model.Hand;
import Model.Card;
import Model.Game;
import Network.Network;
import java.util.HashMap;




/**
 * Note 2 players, you are player 2
 * 3,4 players, you are player 3
 * with 5 players, you are player 4
 */



public class Controller {

    private static Model    model;
    private static Controller controller;
    public Network          net;

    private final int       maxInvalids = 3; // starts at 0, 3rd in a row cancels the game

    private int             invalidInfo;
    private int             invalidDiscard;
    private int             invalidPlay;


    private int             myPosition;// this is a dumby
    private HashMap <Integer, Integer> truePosition;

    private boolean         gameCancelled;
    private boolean         gameEnd;

    private boolean         yourTurn;


    public Controller (Model model) {
        this.model = model;
        this.invalidInfo = 0;
        this.invalidDiscard = 0;
        this.invalidPlay = 0;
        this.setMyPosition();
        this.controller = this;
        this.truePosition = new HashMap<>();
    }

    public Controller (Model model, int myPosition) {
        this(model);
        this.myPosition = myPosition; // to be determined
    }

    /**
     * returns your position at the table
     * @return your position at the table
     */

    public int getMyPosition () {
        return myPosition;
    }

    /**
     * Sets the position of the player with accordance to the  FXML launched
     * depending on game size.
     * Every position corrosponds to the bottom position on the table
     */

    public void setMyPosition () {

        if (this.model.getGame() == null) {
            this.myPosition = 2;    // default 2 for testing purposes
        } else {

            switch (this.model.getGame().getNumberOfPlayers()) {
                case 2:
                    this.myPosition = 2;
                    break;
                case 3:
                    this.myPosition = 3;
                    break;
                case 4:
                    this.myPosition = 3;
                    break;
                case 5:
                    this.myPosition = 4;
                    break;
                default:
                    this.myPosition = 2; // default position use player 1 (debugging)
                    break;
            }
        }
    }

    /**
     * retrieve the true position from the hashmap.
     * True position may change deping on the server
     * dealing hands of cards
     * @param position position in model, mapped to true position
     *                 of the server
     * @return  returns the true position
     */
    public int getTruePosition(int position) {
        return this.truePosition.get(position);
    }

    /**
     * sets the gamePosition (gui and model) mapped to a server position.
     * @param gamePosition  position of players according to the GUI
     * @param serverPosition true position of the player according to the server
     */
    public void setTruePosition(int gamePosition, int serverPosition) {
        this.truePosition.put(gamePosition, serverPosition);
    }

    /**
     * intialize the true position of the hands by looping through the
     * hands of cards delt by the server
     * after game hands have been delt, loop through to find the
     * @param hands the hands
     * @param numberOfPlayers number of players
     */
    public void initTruePosition ( String [][] hands, int numberOfPlayers) {
        int emptySeat ;
        for (emptySeat = 0; emptySeat < numberOfPlayers; emptySeat++) {
            if ( hands[emptySeat][0] == null) {
                break;
            }
        }

        if( emptySeat + 1 != this.getMyPosition() ) {

            this.setTruePosition( this.getMyPosition(), emptySeat + 1);
            this.setTruePosition( emptySeat + 1, this.getMyPosition() );
        }

        int fillTrues;
        for (fillTrues = 1; fillTrues <= numberOfPlayers; fillTrues++) {
            this.truePosition.putIfAbsent(fillTrues, fillTrues);
        }

    }

    /**
     * Obtain the status of gameCancelled
     * @return status of gameCancelled
     */
    public boolean  getGameCancelledStatus () {
        return this.gameCancelled;
    }

    /**
     * Obtaint the current invalid information
     * @return the current invalid information
     */
    public int getInvalidInfo () {
        return invalidInfo;
    }

    /**
     * increment the invalid info by 1
     */
    private void incrementInvalidInfo () {
        this.invalidInfo += 1;
    }

    /**
     * reset the counter on a valid information
     */
    private void resetInvalidInfo () {
        this.invalidInfo = 0;
    }

    /**
     * @return retrieve how many invalid discard there are
     */
    public int getInvalidDiscard () {
        return invalidDiscard;
    }

    /**
     * increment the invaliddDiscard by 1
     */
    private void incrementInvalidDiscard () {
        this.invalidDiscard += 1;
    }

    /**
     * reset the invalidDiscard upon properly discarding
     */
    private void resetInvalidDiscard () {
        this.invalidDiscard = 0;
    }

    /**
     * obtain the invalid play
     * @return how many invalid plays have occurred
     */
    public int getInvalidPlay () {
        return this.invalidPlay;
    }

    /**
     * increment the invalidPlay by 1
     */
    private void incrementInvalidPlay () {
        this.invalidPlay += 1;
    }

    /**
     *reset the invalidPlay counter
     */
    private void resetInvalidPlay () {
        this.invalidPlay = 0;
    }

    /**
     *checks if you give a vlaid rank hint to a player
     * @param playerID player's id that will recieve the hint
     * @param rank of card for the hint
     */
    public void checkHint (int playerID, int rank) {
        for (Player p : this.model.getPlayers()) {
            if(p.getPlayerId() == playerID && p.getHand().countCardsByRank(rank) <= 0){
                this.invalidInfo++;
                this.gameCancelled = (this.invalidInfo == maxInvalids);
            }
        }
    }

    /**
     *checks if you send a valid hint to a selected player
     * @param playerID id of player to give the suit hint to
     * @param suit suit of the card
     */
    public void checkHint (int playerID, char suit) {
        for (Player p : this.model.getPlayers()) {
            if(p.getPlayerId() == playerID && p.getHand().countCardsBySuit(suit) <= 0){
                this.invalidInfo++;
                this.gameCancelled = (this.invalidInfo == this.maxInvalids);
            }
        }
    }

    /**
     * checks for a valid position in a players hand befor discarding
     * @param cardPosition check for a valid card position
     * @param playerID id of player to discard the card from
     */
    public void checkDiscard (int cardPosition, int playerID) {

        for (Player p : this.model.getPlayers()) {
            if (p.getPlayerId() == playerID){
                this.invalidDiscard += (cardPosition < p.getHand().getSize() && cardPosition >= 0
                        && this.model.getHintToken().getHintToken() < this.model.getHintToken().MAX) ? 0 : 1;
                this.gameCancelled = (this.invalidDiscard == this.maxInvalids);
            }
        }
    }

    /**
     * increase invalid play if picked card doesnt exist ie play r5,
     * but doesn't exist in player hand
     * sets game cancelled flag if 3x invalid plays occur
     * Do a CheckPlay() followed by a getInvalidPlays() to use
     * @param cardPosition the card positions
     * @param playerID the player's ID
     */
    public void checkPlay (int cardPosition, int playerID) {
         /* TODO
                CHECK IF THE CARD SELECTED FROM THE MENU EXISTS IN THE PLAYERS HAND
         */
          for (Player p : this.model.getPlayers()) {
              if ( p.getPlayerId() == playerID ){
                  this.invalidPlay += (cardPosition < p.getHand().getSize() && cardPosition >= 0) ? 0 : 1;
                  this.gameCancelled = (this.invalidPlay == this.maxInvalids);
              }
          }

    }

    /**
     *puts cards onto the table stack if a correct play is made.
     *Will burn a card if the card is not approriately placed onto the build stack
     * @param cardPosition, this is the position of card to be played by the user
     * Card position is index at 0 to 1 - cardhand size
     * @param myPosition my positiotn
     */

    public void playCard (int myPosition, int cardPosition) {

        for (Player p : this.model.getPlayers()) {
            if (p.getPlayerId() == myPosition) {
                char playSuit = p.getHand().getCard(cardPosition).getSuit();
                int playRank = p.getHand().getCard(cardPosition).getRank();

                if ( this.model.getPlayCardPile().get(playSuit)  == playRank - 1 ) {
                     Card playCard = p.getHand().getCard(cardPosition);
                     this.model.pushPlayPiles(playCard); // put the new card onto the deck
                    //TODO Construct JSON

                }else {
                    this.model.getFuseToken().decreaseFuseToken();
                }

            }
        }

    }

    /**
     * Burn the selected in your hand.
     * Befor using this method use a checkDiscard method, and check
     * for 3x invalid discard in a row
     * @param myPosition my position
     * @param cardPosition the card position in your hand that is being burned
     */
    public void burnCard (int myPosition, int cardPosition) {
        for (Player p : this.model.getPlayers()) {
            if (p.getPlayerId() == this.getMyPosition()) {
                Card discardCard = p.getHand().getCard(cardPosition);
                this.model.addDiscardPile(discardCard); // add the discard card to the discard pile
                this.model.getHintToken().increaseHintToken();  // increase the hint token
                // TODO construct the discard action to server
            }
        }
    }

    /**
     * decrease the current hint token count of the game
     * hint based on suit
     * @param playerID player id to give a hint
     * @param suit suit of the hint
     */
    public void giveHint (int playerID, char suit) {

        this.model.getHintToken().decreaseHintToken();

        //TODO contruct the inform action action to another player
    }

    /**
     * deases the current hint token count of the game
     * hint based on rank
     * @param playerID the player's ID
     * @param rank the rank
     */
    public void giveHint (int playerID, int rank) {
        this.model.getHintToken().decreaseHintToken();
        //TODO contruct the inform action action to another player
    }

    /**
     * Replace card in the players hand with a new card that is drawn
     * @param cardPosition  card positin in hand that is being replaced
     * @param replacementCard   replacement card to be inserted into the hand
     */
    public void replaceCard (int cardPosition, Card replacementCard) {
        for (Player p : this.model.getPlayers()) {
            if (p.getPlayerId() == this.getMyPosition()){
                p.receiveCard(cardPosition,replacementCard);    // card to be replaced in hand
            }
        }
    }



    /**
     * Gets the appropriate controller object based on the name of the controller
     * @param controllerName - String : Name of the controller
     * @return Object : A controller object that implements Initializable to act as a JavaFX controller
     */
    public static Object getController (String controllerName) {
        if (model != null) {
            switch (controllerName) {
            case "Controller":
                return controller;
            case "InitialWindowController":
                return new InitialWindowController();
            case "JoinGameController":
                return new JoinGameController(model);
            case "CreateGameController":
                return new CreateGameController(model);
            case "LobbyController":
                return new LobbyController(model);
            case "MoveSelectionController":
                return new MoveSelectionController(model);
            case "HintSelectionController":
                return new HintSelectionController(model);
            case "GameController":
                return new GameController(model);
            default:
                throw new RuntimeException("Controller not found, or the controller does not need a model reference!");
            }
        }
        /* This statement block should never reach, as this method although static should never be called
        *  before initializing this class with a reference to model when following MVC design */
        throw new RuntimeException("Controller class is not initialized");
    }

    public static void main (String [] args) {

        /* loading the model with a player and hand to test */
        Model testModel = new Model();

        Hand testHand1 = new Hand(new String [] {"r1", "b5", "w1", "w4", "y2"}, 5);
        Hand testHand2 = new Hand(new String [] {"b1", "w5", "g1", "r4", "r2"}, 5);
        Hand testHand3 = new Hand(new String [] {"w1", "y5", "r1", "y4", "b2"}, 5);

        Player testPlayer1 = new Player(testHand1,1);
        Player testPlayer2 = new Player(testHand2,2);
        Player testPlayer3 = new Player(testHand3,3);
        testModel.addPlayer(testPlayer1);
        testModel.addPlayer(testPlayer2);
        testModel.addPlayer(testPlayer3);


        Controller testController = new Controller(testModel);

        /* testing the invlalidPlay() getters */
        assert (testController.getInvalidPlay() == 0) : "getInvalid play has failed, should be zero and is :"+
                testController.getInvalidPlay();

        /* testing the invalidInfo() getting */
        assert (testController.getInvalidInfo() == 0 ) : "failed the invalid info testing, value should be 0";

        /* testing the invalidDiscard() getter */
        assert (testController.getInvalidDiscard() == 0 ) : "failed the invalid discard testing value should be 0";

        /* testing the checkPlay() function*/

        /* test to play a valid card */
        testController.checkPlay(1,1);
        assert (testController.getInvalidPlay() == 0 ) : "correct hint, valid play counter should not increment" +
                "recieved :" + testController.getInvalidPlay();

        testController.checkPlay(6,1);
        assert (testController.getInvalidPlay() == 1 ) : "first invalid hint, invalidHints should be at 1";

        testController.checkPlay(-1, 1);
        assert (testController.getInvalidPlay() == 2 ) : "second invalid hint, invalidHints should be at 2";

        testController.checkPlay(7, 1);
        assert (testController.getInvalidPlay() == 3 ) : "third invalid hint card position out of range";

        assert (testController.getGameCancelledStatus()) : "gameEnd flag should be risen after 3 invalids";

        testController.gameCancelled = false;
        assert( !testController.getGameCancelledStatus()) : "flag should be false, failed to reset";

        testController.checkHint(2,'w');
        assert (testController.getInvalidInfo() == 0) : "valid hint no raise in count should occur";

        testController.checkHint(3,3);
        assert (testController.getInvalidInfo() == 1): "failed give give valid information based on rank";

        testController.checkHint( 3,'m');
        assert (testController.getInvalidInfo() == 2) : "failed to increment hintInvalid based on suit";

        testController.checkHint( 3,'g');
        assert (testController.getInvalidInfo() == 3) : "failed to increment invalid hint based on rank";


        assert(testController.getGameCancelledStatus()) : "game cancelled status should be raised";

        testController.gameCancelled = false;
        assert (!testController.getGameCancelledStatus()) : "failed to reset the gameCanelled flag after testing";

        testController.model.getHintToken().decreaseHintToken();
        testController.checkDiscard( 1,1);
        assert (testController.getInvalidDiscard() == 0) : "failed, icremented invalidDiscard when it shouldn't have";

        testController.model.getHintToken().increaseHintToken();

        testController.checkDiscard(3,2);
        assert (testController.getInvalidDiscard() == 1 ) : "increase in invalid discard didn't occur when it should have";

        testController.checkDiscard(-1,1);
        assert (testController.getInvalidDiscard() == 2 ) : "increase in invalid discard should have occured";

        testController.checkDiscard(7,1);
        assert (testController.getInvalidDiscard() == 3) : "increase in invalid discard should have occured";


        /* test to play the card, and increase the play card pile */

        Model test2Model = new Model();

        Hand test2Hand1 = new Hand(new String [] {"r1", "b5", "w1", "w4", "y2"}, 5);
        Hand test2Hand2 = new Hand(new String [] {"b1", "w5", "g1", "r4", "r2"}, 5);

        Card card1 = new Card(1,'r');
        Card card2 = new Card(2,'r');
        Card card3 = new Card(5,'w');


        Player test2Player1 = new Player(test2Hand1,1);
        Player test2Player2 = new Player(test2Hand2,2);
        test2Model.addPlayer(test2Player1);
        test2Model.addPlayer(test2Player2);

//        test2Model.pushPlayPiles(card1);
//        test2Model.pushPlayPiles(card2);
//        test2Model.pushPlayPiles(card3);

        Controller test2Controller = new Controller(testModel);


        /* test to check for discard pile to grow */

        test2Controller.playCard(2,0);
        assert test2Controller.model.getPlayCardPile().get('b') == 1 : "playcard pile has increase incorrectly value " +
                                                                      "should be 1 and gave "+
                                                                       test2Controller.model.getPlayCardPile().get('r');

        test2Controller.playCard(2,1);
        assert test2Controller.model.getFuseToken().getFuseToken() == 2 : "decrement fuse token for an invalid" +
                                                                          "card card play selection";

        /* test of a valid discard */

        test2Controller.checkHint(2,3);
        test2Controller.burnCard(2,4);
        assert test2Controller.model.getHintToken().getHintToken() == 8 : "burn a card and revive the hint count to 8";



                                                                //relative value     true value
        String[][] testHands = {{"r1", "b1", "w1", "y1", "g1"},    // p1               p1
                                {"r2", "b2", "w2", "y2", "g2"},    // p2               p2
                                {"r3", "b3", "w3", "y3", "g3"},    // p3               p4
                                {null, null, null, null, null}};    // p4               p3



        Model test3Model = new Model(4, 60, "hi", "token", "fsdt",
                            "m", 4);

        Controller controller3 = new Controller(test3Model);

        controller3.model.getGame().setNumberOfPlayers(4);

        controller3.initTruePosition(testHands, 4);
        assert controller3.getTruePosition(1) == 1 : "1st position is wrong";
        assert controller3.getTruePosition(2) == 2 : "2st position is wrong";
        assert controller3.getTruePosition(3) == 4 : "3rd position is wrong";
        assert controller3.getTruePosition(4) == 3 : "4th position is wrong";



                                                                  //relative value     true value
        String[][] testHands2 = {{"r1", "b1", "w1", "y1", "g1"},  //1                   1
                                 {null, null, null, null, null},                        //4                   2
                                {"r2", "b2", "w2", "y2", "g2"},   //3                   3
                                {"r3", "b3", "w3", "y3", "g3"},    //2                  4
                                {"r4", "b4", "w4", "y4", "g4"}};  //5                  5

        Model test4Model = new Model(5, 60, "hi", "token", "fsdt",
                "m", 5);

        Controller controller4= new Controller(test4Model);

        controller4.initTruePosition(testHands2, 5);
        assert controller4.getTruePosition(1) == 1 : "1st position is wrong";
        assert controller4.getTruePosition(2) == 4 : "2st position is wrong";
        assert controller4.getTruePosition(3) == 3 : "3st position is wrong";
        assert controller4.getTruePosition(4) == 2 : "4st position is wrong";
        assert controller4.getTruePosition(5) == 5 : "5st position is wrong";




        System.out.println("Controller testing completed");
    }

}

















