Unfortunately, there are a couple of incomplete features. First, the hint feature is not able to work yet. We do have the "hint" button
in the game but it does nothing. We do have all the codes for the hint in our program but they are not connected yet. Second, there 
is no AI integration, but all the functions for AI are finished, just missing a button to turn it on. Third, our project works fine
with "Wild" and "Normal" mode but not the "rainbow" mode, the server is giving back an error message if we pick "rainbow" when we
are creating the game. The known bug found is that we can not discard the rightmost card in the hand. There are no workarounds we
have yet and we are trying to fix it until the end of the due day.