package Model;

import java.util.HashMap;

public class Hand {

    private HashMap<Integer, Card> cards;
    private int numCards;

    /**
     * Create a new hand of cards
     * @param cards The cards starting in the hand
     * @param numCards The number of cards in hand
     */
    public Hand(String[] cards, int numCards){

        //checking if the size of the array is the same as the expected size of the hand
        assert cards.length == numCards;

        this.cards = new HashMap<Integer, Card>();
        for (int i = 0; i < cards.length; i++){
            this.cards.put(i, cards[i] != null ? new Card(cards[i]) : null);
        }
        this.numCards = numCards;
    }

    /**
     * This function returns the number of cards in hand
     * @return the number cards in hand
     */
     public int getSize(){
        return this.numCards;
     }

    /**
     * This function returns the card at the position in the hand
     * @param position int positon in the hand
     * @return a card at the position in the hand
     */
    public Card getCard(int position) {
        if (!this.cards.containsKey(position)) {
            throw new RuntimeException("Invalid card position in hand from getCard");
        }
        return this.cards.get(position);
    }

    /**
     * Add a card to an existing spot in the hand
     * @param position The position the card will go in
     * @param card The Card to be place in hand
     */
    public void receiveCard(int position, Card card){
        if (!this.cards.containsKey(position)){
            throw new RuntimeException("Hand does not have a " + Integer.toString(position) + "the card");
        }
        this.cards.put(position, card);
    }


    /**
     * Count the number of cards in hand with the given rank
     * @param rank The rank to check
     * @return The number of cards with the given rank
     */
    public int countCardsByRank(int rank){
        int count = 0;
        for (Card card : this.cards.values()){
            if (card != null && card.checkRank(rank)){
                count++;
            }
        }
        return count;
    }

    /**
     * Count the number of cards in hand with the given suit
     * @param suit The suit to check
     * @return The number of cards with the given suit
     */
    public int countCardsBySuit(char suit){
        int count = 0;
        for (Card card : this.cards.values()){
            if (card != null && card.checkSuit(suit)){
                count++;
            }
        }
        return count;
    }

    /**
     * Count the number of cards matching the given string
     * @param card The card to check for
     * @return The number of matching cards
     */
    public int countCard(String card){
        int count = 0;
        for (Card c : this.cards.values()){
            if (c != null && c.toString().equals(card)){
                count++;
            }
        }
        return count;
    }
    /**
     * Create a readable string representation of the hand
     * @return The string representation of the hand
     */
    public String toString(){
        String result = "";

        for (Card card : this.cards.values()){
            result += (card != null) ? card.toString() + ", " : "null, ";
        }
        //Trim the last ", " off the string
        return result.substring(0, result.length()-2);
    }

    public static void main(String[] args){
        Hand testHand = new Hand(new String[] {"b1","b2","b3","b4","b5"},5);

        //testing getSize
        if (testHand.getSize() != 5){
            System.out.println("Error with getSize");
        }

        //testing countCardsBySuit
        if (testHand.countCardsBySuit('b') != 5) {
            System.out.println("Error with countCardsBySuit: Positive test");
        }

        if (testHand.countCardsByRank('r') != 0) {
            System.out.println("Error with countCardsBySuit: Negative test");
        }

        //Testing countCardsByRank
        if (testHand.countCardsByRank(1) != 1) {
            System.out.println("Error with countCardsByRank: Positive test");
        }

        if (testHand.countCardsByRank(-1) != 0) {
            System.out.println("Error with countCardsByRank: Negative test");
        }

        if (!testHand.toString().equals("b1, b2, b3, b4, b5")) {
            System.out.println("Error with toString");
        }

        //testing getCard
        try {
            testHand.getCard(11);
            System.out.println("getCard did not throw an exception when it should have");
        } catch (Exception e){}

        try {
            if( !testHand.getCard(1).checkCard('b',2)) {
                System.out.println("Error with hand's getCard");
            }
        } catch (Exception e){
            System.out.println("getCard threw exception when it shouldn't have");
        }

        //testing receiveCard
        try {
            testHand.receiveCard(1, new Card("y1"));
        } catch (Exception e) {
            System.out.println("recieveCard threw exception when it shouldn't have");
        }

        try {
            testHand.receiveCard(9, new Card("y1"));
            System.out.println("receiveCard did not throw an exception when it should have");
        } catch (Exception e) {}


        try {
            if( !testHand.getCard(1).checkCard('y',1)) {
                System.out.println("Error with hand's getCard");
            }
        } catch (Exception e){
            System.out.println("getCard threw exception when it shouldn't have");
        }

        testHand.receiveCard(1, new Card("b1"));
        if (testHand.countCard("b3") != 1) {
            System.out.println("Error with countCard");
        }
        if (testHand.countCard("b1") != 2) {
            System.out.println("Error with countCard");
        }
        if (testHand.countCard("r1") != 0) {
            System.out.println("Error with countCard");
        }
    }
}
