package KeyValuePair;

public class KeyValuePair {

    private String key;
    private String value;

    public KeyValuePair (String key, String value){
        this.key = key;
        this.value = value;
    }

    /**
     * Obtain the key
     * @return the key as a string
     */
    public String getKey (){
        return this.key;
    }

    /**
     * obtain the value
     * @return the value as a string
     */
    public String getValue (){
        return this.value;
    }

    @Override
    public String toString() {
        return (key+":"+value);
    }

    /**
     * set the key
     * @param key the key as a string
     */
    public void setKey (String key) {
        this.key = key;
    }

    /**
     * set the value
     * @param value the value as a string
     */
    public void setValue (String value) {
        this.value = value;
    }

    /**
     * set the pair
     * @param key the key as a string
     * @param value the value as a string
     */
    public void setPair (String key, String value){
        this.key = key;
        this.value = value;
    }

    public static void main(String[] argv){

        //Testing constructor
        KeyValuePair KVP = new KeyValuePair("Hello","World");

        //testing getKey
        if (!KVP.getKey().equals("Hello")) {
            System.out.println("Problem with getKey or Constructor");
        }

        //testing getValue
        if (!KVP.getValue().equals("World")){
            System.out.println("Problem with getValue or Constructor");
        }

        //testing toString
        if (!KVP.toString().equals("Hello:World")){
            System.out.println("Problem with toString");
        }

        //testing setKey
        KVP.setKey("Foo");

        if (!KVP.getKey().equals("Foo")) {
            System.out.println("Problem with setKey");
        }

        //testing setValue
        KVP.setValue("Bar");

        if (!KVP.getValue().equals("Bar")){
            System.out.println("Problem with setValue");
        }

        //testing setPair
        KVP.setPair("Computer", "Science");

        if (!KVP.toString().equals("Computer:Science")){
            System.out.println("Problem with setPair");
        }

        return;
    }

}




