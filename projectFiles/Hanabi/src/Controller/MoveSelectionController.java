package Controller;

import Model.Model;
import View.View;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MoveSelectionController implements Initializable {

    @FXML
    private Button playCardButton;

    @FXML
    private Button giveHintButton;

    @FXML
    private Button discardButton;

    private Model model;

    public MoveSelectionController (Model model) {
        this.model = model;
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {

        playCardButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                GameController.moveSelection = "playCard";
                ((Node) event.getSource()).getScene().getWindow().hide(); // close this window
            }
        });

        giveHintButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage stage = new Stage();
                Node node = (Node) event.getSource();

                Object controller = Controller.getController("HintSelectionController");
                Parent parent = View.loadXML("HintSelection", controller);

                stage.setResizable(false);
                stage.centerOnScreen();
                stage.setScene(new Scene(parent));
                stage.setTitle("Give a Hint");
                stage.show();

                node.getScene().getWindow().hide(); // close this window after opening the new one
            }
        });

        discardButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                GameController.moveSelection = "discardCard";
                ((Node) event.getSource()).getScene().getWindow().hide(); // close this window
            }
        });

    }
}
