package Model;

import org.json.simple.JSONObject;
import java.util.HashMap;
import java.lang.Math;
import java.util.HashSet;

public class AIModel extends Player {

    //Settings of the game important for probability
    private String rainbowMode;
    private int cardsInGame;

    //Keeping track of importance of cards
    private HashMap<String, Integer> cardImportance;

    //Keep track of which cards may be in hand
    private HashMap<Integer, HashMap<String, Integer>> handConstraints;

    //Keep track of values throughout the game
    private HashMap<Integer, HashSet<JSONObject>> recentHints;
    private HashMap<String, Integer> discardedCards;

    //TODO: Used for testing, remove once model and AImodel can be properly connected
    private Model model;

    /**
     * Create a model for the AI
     *
     * @param cardsInGame The total number of cards in the game
     * @param rainbowMode The mode that dictates rainbow cards
     * @param hand        The hand the AI has
     * @param playerId    The AI's player id
     */
    public AIModel(int cardsInGame, String rainbowMode, Hand hand, int playerId) {

        //Initialize the player superclass
        super(hand, playerId);

        //Set and initialize fields
        this.cardsInGame = cardsInGame;
        this.rainbowMode = rainbowMode;
        this.recentHints = new HashMap<Integer, HashSet<JSONObject>>();
        this.cardImportance = new HashMap<String, Integer>();
        this.discardedCards = new HashMap<String, Integer>();
        this.handConstraints = new HashMap<Integer, HashMap<String, Integer>>();

        //Fill in the initial constraints for each card in hand
        for (int i = 0; i < this.hand.getSize(); i++) {
            HashMap<String, Integer> constraints = new HashMap<String, Integer>();

            //Enumerate possible cards
            for (char suit : Card.getValidSuits()) {
                if (suit != 'm' || !this.rainbowMode.equals("none")) {

                    //Add the card with the appropriate number to the constraints
                    for (int ranks : Card.getValidRanks()) {
                        int numberOfCards = 2;
                        if (ranks == 1) {
                            numberOfCards = 3;
                        } else if (ranks == 5) {
                            numberOfCards = 1;
                        }
                        constraints.put(suit + Integer.toString(ranks), numberOfCards);

                        //Add the card's importance
                        cardImportance.put(suit + Integer.toString(ranks),
                                (numberOfCards != 3 ? (numberOfCards == 2 ? 1 : 2) : 3));
                    }
                }

                //Add the constraints for card i to the hand constraints
                this.handConstraints.put(i, constraints);
            }
        }
    }

    /**
     * Get the card importances
     * @return The card importances
     */
    public HashMap<String, Integer> getCardImportance() {
        return cardImportance;
    }

    /**
     * Get the hand constraints
     * @return The hand constraints
     */
    public HashMap<Integer, HashMap<String, Integer>> getHandConstraints() {
        return handConstraints;
    }

    /**
     * Get the recent hints
     * @return The recent hints
     */
    public HashMap<Integer, HashSet<JSONObject>> getRecentHints() {
        return recentHints;
    }

    //TODO: Change how the model is references when proper procedures are set up
    /**
     * Determine the total number of cards unseen by the AI
     * @return The numder of unseen cards
     */
    private int totalCardsUnseen() {

        //Determine the amount of cards in players's hands
        int cardsInPlayersHands = 0;
        for (Player p : this.model.getPlayers()) {
            cardsInPlayersHands += p.getHand().getSize();
        }

        //Return the number of unseen cards
        return this.cardsInGame - this.discardedCards.values().stream().mapToInt(i->i).sum() -
                this.model.getTotalScore() - cardsInPlayersHands + this.hand.getSize();
    }

    //TODO: Change how the model is references when proper procedures are set up
    /**
     * Checks how many of the given card have not been seen
     * @param card the card that needs to be checked
     * @return the number of the given card as an int
     */
    private int numCardUnseen(String card){

        //Determine the total number of the card in the game
        Card c = new Card(card);
        int total = 1;
        if (c.getRank() == 1) {
            total++;
        } else if (c.getRank() == 5) {
            total--;
        }
        total++;

        //Lower the total by all of the matching cards seen
        if (!this.rainbowMode.equals("wild") && this.model.getScoreByColor(c.getSuit()) >= c.getRank()) {
            total--;
        } else if (this.rainbowMode.equals("wild") && c.getSuit() != 'm' &&
                this.model.getScoreByColor(c.getSuit()) >= c.getRank()){
            if (this.model.getWildRainbowPlays().get(c.getSuit()) == null ||
                    !this.model.getWildRainbowPlays().get(c.getSuit()).
                            contains(Character.getNumericValue(card.charAt(1)))){
                total--;
            }
        } else if (this.rainbowMode.equals("wild") && card.charAt(0) == 'm'){
            for (char suit : this.model.getWildRainbowPlays().keySet()){
                total -= this.model.getWildRainbowPlays().get(suit).stream().mapToInt(i->i).sum();
            }
        }
        if (this.discardedCards.containsKey(card)) {
            total -= this.discardedCards.get(card);
        }
        for (Player p : this.model.getPlayers()) {
            total -= p.hand.countCard(card);
        }

        return total;
    }

    //TODO: change reference to model
    /**
     * Determine how many of the given card have not been discarded/played
     * @param card The card to check
     * @return The number of cards remaining in hands/draw pile
     */
    private int numCardLeft(String card){
        int total = 0;
        for (Player p : this.model.getPlayers()) {
            total += p.hand.countCard(card);
        }
        return total + numCardUnseen(card);
    }

    /**
     * Receive a hint about AI's cards
     * @param hint The hint that the AI receives
     */
    public void receiveHint(JSONObject hint){
        boolean[] indices = (boolean[])hint.get("cards");
        for (int i = 0; i < indices.length; i++){

            //Recalculate the hand constraints for the card at index i if it was included in the hint
            if (indices[i]){
                this.handConstraints.get(i).replaceAll(
                        (k, v) -> ((hint.get("suit") != null && ((String)hint.get("suit")).charAt(0) != k.charAt(0))
                                || (hint.get("rank") != null
                                && (int)hint.get("rank") != Character.getNumericValue(k.charAt(1)))) ? 0 : v);
            } else {
                this.handConstraints.get(i).replaceAll(
                        (k, v) -> ((hint.get("suit") != null && ((String)hint.get("suit")).charAt(0) == k.charAt(0))
                                || (hint.get("rank") != null
                                && (int)hint.get("rank") == Character.getNumericValue(k.charAt(1)))) ? 0 : v);
            }
        }
    }

    /**
     * Add a hint a different player received to recent hints
     * @param hint The hint that was given
     */
    public void updateRecentHints(JSONObject hint){
        this.recentHints.putIfAbsent((Integer)hint.get("player"), new HashSet<JSONObject>());
        this.recentHints.get(hint.get("player")).add(hint);
    }

    /**
     * Adjust hand constraints based on starting hands of other players
     * @param hand The hand of another player
     */
    public void otherPlayersStartingHand(Hand hand){
        for (int i = 0; i < hand.getSize(); i++){
            Card card = hand.getCard(i);
            this.updateHandConstraints(card);
        }
    }

    @Override
    public void receiveCard(int position, Card card){
        this.hand.receiveCard(position, card);

        //Reset the hand constraints
        this.handConstraints.get(position).replaceAll((k, v) -> this.numCardUnseen(k));
    }

    /**
     * Remove all recent hints that would now give new information to the player that drew and
     * Update hand constraints now that another card has been seen
     * @param card The card the player drew
     * @param player The player who drew the card
     */
    public void playerDrewCard(Card card, int player){

        //Remove all recent hints to the player that either contain the suit or the rank of the card that was drawn
        this.recentHints.get(player).removeIf(
                json -> (json.get("suit") != null && (((String)json.get("suit")).charAt(0) == card.getSuit()))
                        || (json.get("rank") != null && (Integer) json.get("rank") == card.getRank()));
        this.updateHandConstraints(card);
    }

    /**
     * Update the hand constraints now that the given card has been seen
     * @param card The card that was seen
     */
    private void updateHandConstraints(Card card){
        for (int i = 0; i < this.handConstraints.size(); i++){
            handConstraints.get(i).computeIfPresent(card.toString(), (k, v) -> v == 0 ? 0 : v - 1);
            
            if (this.handConstraints.get(i).get(card.toString()) < 0){
                throw new RuntimeException("AI's hand constraints went below 0 for some card");
            }
        }
    }

    //TODO: Change reference to model
    /**
     * Update the discarded cards with the given card and adjust card importance appropriately
     * @param card The card that was discarded
     */
    public void updateDiscardedCards(Card card){
        this.discardedCards.compute(card.toString(), (k, v) -> (v == null) ? 1 : (v + 1));

        if (this.cardImportance.get(card.toString()) == 0){
            return;
        }

        //Check whether there are 0 of card left in the game and 0 rainbow cards of the same rank if they are wild
        Card rainbowVer = new Card(card.getRank(), 'm');
        if (this.numCardLeft(card.toString()) == 0 && !this.rainbowMode.equals("wild") &&
                this.model.getScoreByColor(card.getSuit()) < card.getRank()
                || (this.rainbowMode.equals("wild")
                && this.numCardLeft(rainbowVer.toString()) == 0 && !card.toString().equals(rainbowVer.toString()))){

            //Set the importance of all cards with the same suit and greater rank to 0
            this.cardImportance.replaceAll((k, v) -> (k.charAt(0) == card.getSuit() &&
                    Character.getNumericValue(k.charAt(1)) >= card.getRank()) ? 0 : v);

        } else if (this.numCardLeft(card.toString()) == 0){
            this.cardImportance.replace(card.toString(), 0);
        } else {
            this.discardHelper(card);
        }

        //Update lower rank cards whose importance may depend on higher rank cards
        StringBuilder lowerRanks = new StringBuilder(card.toString());
        int j = 0;
        do{
            for (int i = card.getRank() - 1; i > 0; i--){
                lowerRanks.replace(1, 2, Integer.toString(i));
                this.discardHelper(new Card(lowerRanks.toString()));
            }

            if (j == Card.getValidSuits().length){
                break;
            }

            lowerRanks.setCharAt(0, Card.getValidSuits()[j]);
            lowerRanks.replace(1, 2, Integer.toString(card.getRank()));
            j++;
        } while (this.rainbowMode.equals("wild") && card.getSuit() == 'm'
                && j <= Card.getValidSuits().length);

    }

    /**
     * Help update card importance when a card is discarded
     * @param card The card to update the importance of
     */
    private void discardHelper(Card card){
        int numCards = 1;
        if (card.getRank() == 1){
            numCards++;
        } else if (card.getRank() == 5){
            numCards--;
        }
        numCards++;

        if (this.discardedCards.getOrDefault(card.toString(), 0) == numCards - 1){
            if (card.getRank() <= 3){

                /* Set the importance to be the sum of the number of cards left that have the same suit that are
                not on the stack, not including cards that can never be played.*/
                StringBuilder sameSuit = new StringBuilder(card.toString());
                int total = 0;
                int i = 1;
                int suitScore = (this.rainbowMode.equals("wild") && card.getSuit() == 'm') ? 0 :
                        this.model.getScoreByColor(card.getSuit());
                sameSuit.replace(1, 2, Integer.toString(suitScore + i));
                StringBuilder rainbowVer = new StringBuilder(sameSuit.toString());
                rainbowVer.setCharAt(0, 'm');

                while (Character.getNumericValue(sameSuit.charAt(1)) <= 5 &&
                        (this.numCardLeft(sameSuit.toString()) > 0 ||
                                (this.rainbowMode.equals("wild") && this.numCardLeft(rainbowVer.toString()) > 0))){

                    total += this.numCardLeft(sameSuit.toString());
                    total += this.rainbowMode.equals("wild") ? this.numCardLeft(rainbowVer.toString()) : 0;
                    i++;
                    sameSuit.replace(1, 2, Integer.toString(suitScore + i));
                    rainbowVer.replace(1, 2, Integer.toString(suitScore + i));
                }

                //Ternary makes sure to not get rid of importance if this card is the next to go on stack
                this.cardImportance.replace(card.toString(),
                        (card.getRank() == suitScore + 1) ? (total + suitScore) : total);

            } else if (card.getRank() == 4){

                //Increment importance of a card of rank 4 if rank 5 card can still be played, otherwise decrement
                StringBuilder rank5 = new StringBuilder(card.toString());
                rank5.replace(1, 2, "5");

                this.cardImportance.compute(card.toString(),
                        (k, v) -> v + ((this.rainbowMode.equals("wild") && this.numCardLeft("m5") > 0) ?
                                (Math.max((int)Math.pow(-1, this.numCardLeft(rank5.toString()) + 1), 0) + 1) :
                                Math.max((int)Math.pow(-1, this.numCardLeft(rank5.toString()) + 1), 0)));
            }
        }
    }

    /**
     * Update the importance of cards when a card is played
     * @param card The card that was played
     */
    public void cardPlayed(Card card){
        if (!this.rainbowMode.equals("wild") || card.getSuit() != 'm'){
            this.cardImportance.replace(card.toString(), 0);

            //Update the importance of the next card
            StringBuilder nextCard = new StringBuilder(card.toString());
            nextCard.replace(1, 2, Integer.toString(card.getRank() + 1));
            this.cardImportance.computeIfPresent(nextCard.toString(), (k, v) -> (v == 0) ? 0 : v + card.getRank());
        } else {
            this.cardImportance.compute(card.toString(), (k, v) -> (this.numCardLeft(card.toString()) == 0) ? 0 : v+1);
        }

    }

    /**
     * Create readable string representation of AI
     * @return String representation of AI
     */
    public String toString(){
        String result = super.toString() + "\n\n";

        result += "Number of cards in game: " + Integer.toString(this.cardsInGame) + "\n";
        result += "Rainbow mode: " + this.rainbowMode + "\n\n";

        result += "Hand contraints:\n";
        for (int i = 0; i < this.handConstraints.size(); i++){
            result += this.handConstraints.get(i).toString() + "\n";
        }

        result += "\nCard importance:\n";
        result += this.cardImportance.toString() + "\n";

        result += "\nDiscarded Cards:\n";
        result += this.discardedCards.toString() + "\n";

        result += "\nRecent Hints:\n";
        for (int i : this.recentHints.keySet()){
            result += "Player " + Integer.toString(i) + ": " + this.recentHints.get(i).toString() + "\n";
        }

        result += "\n";
        return result;
    }

    /**
     * Print a message for testing purposes
     * @param msg The message to print
     */
    private void testingMessage(String msg){
        System.out.println("\n-----------------------------------");
        System.out.println(msg);
        System.out.println("-----------------------------------\n");
    }

    public static void main(String[] args){

        AIModel testAI = new AIModel(50, "none",
                new Hand(new String[]{null, null, null, null, null}, 5), 1);

        testAI.testingMessage("Testing the AIModel with rainbow mode = none");

        System.out.println(testAI.toString());

        //Testing get hand contraints
        if (testAI.getHandConstraints() != testAI.handConstraints){
            System.out.println("Error with get hand constraints, the wrong object was returned");
        }

        //Testing get card importance
        if (testAI.getCardImportance() != testAI.cardImportance){
            System.out.println("Error with get card importance, the wrong object was returned");
        }

        //Testing get recent hints
        if (testAI.getRecentHints() != testAI.recentHints){
            System.out.println("Error, with get recent hints, the wrong object was returned");
        }

        testAI.model = new Model();
        testAI.model.setGame(new Game(2, 10, "Blah", "blah"));
        Player testPlayer = new Player(new Hand(new String[]{"g1", "g2", "g3", "g4", "g5"}, 5), 2);
        testAI.model.setRainbowMode("none");
        testAI.model.addPlayer(testPlayer);
        testAI.model.addPlayer(testAI);

        if (testAI.totalCardsUnseen() != 45){
            System.out.println("Error with totalCardsUnseen, should be 45 instead of " + testAI.totalCardsUnseen());
        }


        testAI.testingMessage("Testing otherPlayersStartingHand");
        testAI.otherPlayersStartingHand(testPlayer.getHand());

        System.out.println(testAI.toString());

        //Testing numCardUnseen
        if (testAI.numCardUnseen("g1") != 2){
            System.out.println("Error with numCardUnseen for g1, should be 2 instead of " + testAI.numCardUnseen("g1"));
        }
        if (testAI.numCardUnseen("r1") != 3){
            System.out.println("Error with numCardUnseen for r1, should be 3 instead of " + testAI.numCardUnseen("r1"));
        }

        //Testing numCardleft
        if (testAI.numCardLeft("g2") != 2){
            System.out.println("Error with numCardLeft for g2, should be 2 instead of " + testAI.numCardLeft("g2"));
        }
        if (testAI.numCardLeft("b1") != 3){
            System.out.println("Error with numCardLeft for b1, should be 3 instead of " + testAI.numCardLeft("b1"));
        }


        testAI.testingMessage("Testing receiveHint (1: rank 2 and 1,2: suit r)");

        JSONObject hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 2);
        hint.put("cards", new boolean[]{true, false, false, false, false});

        testAI.receiveHint(hint);

        System.out.println(testAI.toString());

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "r");
        hint.put("cards", new boolean[]{true, true, false, false, false});

        testAI.receiveHint(hint);

        System.out.println(testAI.toString());


        testAI.testingMessage("Testing receiveCard (pos 1, 2)");

        testAI.receiveCard(0, null);
        System.out.println(testAI.toString());

        testAI.receiveCard(1, null);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing updateRecentHints (to player 2, suit g, rank 3, rank 4)");

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "g");
        hint.put("player", 2);

        testAI.updateRecentHints(hint);
        System.out.println(testAI.toString());

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 3);
        hint.put("player", 2);

        testAI.updateRecentHints(hint);
        System.out.println(testAI.toString());

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 4);
        hint.put("player", 2);

        testAI.updateRecentHints(hint);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing playerDrewCard (player 2, y3, g4)");

        testAI.playerDrewCard(new Card("y3"), 2);
        System.out.println(testAI.toString());

        testAI.playerDrewCard(new Card("g4"), 2);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing updateDiscardedCards (b1, b2, b3, b2)");

        testAI.updateDiscardedCards(new Card("b1"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("b2"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("b3"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("b2"));
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing cardPlayed (w1, w2)");

        testAI.cardPlayed(new Card("w1"));
        System.out.println(testAI.toString());

        testAI.cardPlayed(new Card("w2"));
        System.out.println(testAI.toString());



        /*Testing rainbow mode = rainbow*/



        testAI = new AIModel(60, "firework",
                new Hand(new String[]{null, null, null, null, null}, 5), 1);

        testAI.testingMessage("Testing the AIModel with rainbow mode = firework");

        System.out.println(testAI.toString());

        testAI.model = new Model();
        testAI.model.setGame(new Game(2, 10, "Blah", "blah"));
        testPlayer = new Player(new Hand(new String[]{"g1", "m2", "m3", "g4", "g5"}, 5), 2);
        testAI.model.setRainbowMode("firework");
        testAI.model.addPlayer(testPlayer);
        testAI.model.addPlayer(testAI);

        if (testAI.totalCardsUnseen() != 55){
            System.out.println("Error with totalCardsUnseen, should be 55 instead of " + testAI.totalCardsUnseen());
        }


        testAI.testingMessage("Testing otherPlayersStartingHand");
        testAI.otherPlayersStartingHand(testPlayer.getHand());

        System.out.println(testAI.toString());

        //Testing numCardUnseen
        if (testAI.numCardUnseen("m2") != 1){
            System.out.println("Error with numCardUnseen for m2, should be 1 instead of " + testAI.numCardUnseen("m2"));
        }
        if (testAI.numCardUnseen("m1") != 3){
            System.out.println("Error with numCardUnseen for m1, should be 3 instead of " + testAI.numCardUnseen("m1"));
        }

        //Testing numCardleft
        if (testAI.numCardLeft("m3") != 2){
            System.out.println("Error with numCardLeft for m3, should be 2 instead of " + testAI.numCardLeft("m3"));
        }
        if (testAI.numCardLeft("m1") != 3){
            System.out.println("Error with numCardLeft for m1, should be 3 instead of " + testAI.numCardLeft("m1"));
        }


        testAI.testingMessage("Testing receiveHint (1: rank 2 and 1,2: suit m)");

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 2);
        hint.put("cards", new boolean[]{true, false, false, false, false});

        testAI.receiveHint(hint);
        System.out.println(testAI.toString());

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "m");
        hint.put("cards", new boolean[]{true, true, false, false, false});

        testAI.receiveHint(hint);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing receiveCard (pos 1, 2)");

        testAI.receiveCard(0, null);
        System.out.println(testAI.toString());

        testAI.receiveCard(1, null);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing update recent hints");

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "m");
        hint.put("player", 2);

        testAI.updateRecentHints(hint);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing playerDrewCard (player 2, m1)");

        testAI.playerDrewCard(new Card("m1"), 2);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing updateDiscardedCards (m1, m2, m3, m2)");

        testAI.updateDiscardedCards(new Card("m1"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m2"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m3"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m2"));
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing cardPlayed (m3, m4)");

        testAI.cardPlayed(new Card("m3"));
        System.out.println(testAI.toString());

        testAI.cardPlayed(new Card("m4"));
        System.out.println(testAI.toString());


        /*Testing rainbow mode = wild*/




        testAI = new AIModel(60, "wild",
                new Hand(new String[]{null, null, null, null, null}, 5), 1);

        testAI.testingMessage("Testing the AIModel with rainbow mode = wild");

        System.out.println(testAI.toString());

        testAI.model = new Model();
        testAI.model.setGame(new Game(2, 10, "Blah", "blah"));
        testPlayer = new Player(new Hand(new String[]{"g1", "m2", "m3", "g4", "g5"}, 5), 2);
        testAI.model.setRainbowMode("wild");
        testAI.model.addPlayer(testPlayer);
        testAI.model.addPlayer(testAI);


        if (testAI.totalCardsUnseen() != 55){
            System.out.println("Error with totalCardsUnseen, should be 55 instead of " + testAI.totalCardsUnseen());
        }

        testAI.testingMessage("Testing otherPlayersStartingHand");
        testAI.otherPlayersStartingHand(testPlayer.getHand());

        System.out.println(testAI.toString());

        //Testing numCardUnseen
        if (testAI.numCardUnseen("m2") != 1){
            System.out.println("Error with numCardUnseen for m2, should be 1 instead of " + testAI.numCardUnseen("m2"));
        }
        if (testAI.numCardUnseen("m1") != 3){
            System.out.println("Error with numCardUnseen for m1, should be 3 instead of " + testAI.numCardUnseen("m1"));
        }

        //Testing numCardleft
        if (testAI.numCardLeft("m3") != 2){
            System.out.println("Error with numCardLeft for m3, should be 2 instead of " + testAI.numCardLeft("m3"));
        }
        if (testAI.numCardLeft("m1") != 3){
            System.out.println("Error with numCardLeft for m1, should be 3 instead of " + testAI.numCardLeft("m1"));
        }


        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 2);
        hint.put("cards", new boolean[]{true, false, false, false, false});

        testAI.receiveHint(hint);
        System.out.println(testAI.toString());

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "m");
        hint.put("cards", new boolean[]{true, true, false, false, false});

        testAI.receiveHint(hint);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing receiveCard (pos 1, 2)");

        testAI.receiveCard(0, null);
        System.out.println(testAI.toString());

        testAI.receiveCard(1, null);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing update recent hints");

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "m");
        hint.put("player", 2);

        testAI.updateRecentHints(hint);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing playerDrewCard (player 2, m1)");

        testAI.playerDrewCard(new Card("m1"), 2);
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing updateDiscardedCards (m1, m2, y2, m3, m2)");

        testAI.updateDiscardedCards(new Card("m1"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m2"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("y2"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m3"));
        System.out.println(testAI.toString());

        testAI.updateDiscardedCards(new Card("m2"));
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing cardPlayed (m3, m4)");

        testAI.cardPlayed(new Card("m3"));
        System.out.println(testAI.toString());

        testAI.cardPlayed(new Card("m4"));
        System.out.println(testAI.toString());


        testAI.testingMessage("Testing updateDiscardedCards (w3, w3) and cardPlayed (w2)");

        testAI.updateDiscardedCards(new Card("w3"));
        testAI.updateDiscardedCards(new Card("w3"));
        System.out.println(testAI.toString());

        testAI.cardPlayed(new Card("w2"));
        System.out.println(testAI.toString());
    }
}