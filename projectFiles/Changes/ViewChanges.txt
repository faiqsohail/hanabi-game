View class
	attributes:
		private Stage stage
		private Controller controller
		private static final String XMLPATH

	constructor:
		public View (Stage stage, Controller controller)

	methods:
		public Scene initWindow ()
		returns a new Scene object with the intial window

		public static InputStream getFXML (String fileName)
		returns a InputStream with the specified java fx file

		public static Parent loadXML (String fileName, Object Controller)
		returns a Parent with the specified java fx file and sets it's controller
		with the provided controller object.

		public static Scene createWindow (int numPlayers)
		Creates the game Window for the actual Hanabi game

		public static void showAlert(Alert.AlertType alertType, String header, String alertContent)
		Shows a alert onto the sceeen. Useful for showing messages such as game cancelled game joined, game created.
