package Controller;

import Model.Model;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class HintSelectionController implements Initializable {

    @FXML
    private Button suitButton;

    @FXML
    private Button rankButton;

    private Model model;

    public HintSelectionController (Model model) {
        this.model = model;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        suitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                GameController.moveSelection = "hintSuit";
                ((Node) event.getSource()).getScene().getWindow().hide(); // close this window
            }
        });

        rankButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                GameController.moveSelection = "hintRank";
                ((Node) event.getSource()).getScene().getWindow().hide(); // close this window
            }
        });

    }
}
