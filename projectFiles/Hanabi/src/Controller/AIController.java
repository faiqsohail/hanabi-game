package Controller;

import Model.AIModel;
import Model.Model;
import Model.Card;
import Model.Player;
import javafx.util.Pair;
import org.json.simple.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.BiPredicate;

//For testing
import Model.Hand;
import Model.Game;

public class AIController extends GameController{

    private AIModel aiModel;

    private final float PLAY_CUTOFF = 0.75f;
    private final float DISCARD_CUTOFF = 0.25f;

    public AIController(Model model, AIModel aiModel){
        super(model);
        this.model = model;
        this.aiModel = aiModel;
    }

    /**
     * Calculate the probability that the ith card in hand is a certain card for all cards it could be
     * @param cardIndex The index of the card in hand
     * @return A HashMap of possible cards mapped to their respective probability
     */
    private HashMap<String, Float> calculateCardProb(int cardIndex){

        //Get the constraints for the card in question and calculate the probability for an individual card
        HashMap<String, Integer> constraints = this.aiModel.getHandConstraints().get(cardIndex);
        int numPossibleCards = constraints.values().stream().mapToInt(i->i).sum();
        float probPerCard = 1.0f/numPossibleCards;

        //Create a HashMap of cards to their respective probabilities
        HashMap<String, Float> cardProb = new HashMap<String, Float>();
        for(String card : constraints.keySet()){
            cardProb.put(card, constraints.get(card) * probPerCard);
        }
        return cardProb;
    }

    /**
     * Calculate the importance of a card in hand
     * @param cardIndex The index of the card in hand
     * @return The importance of that card
     */
    private float calculateImportance(int cardIndex){
        HashMap<String, Float> cardProb = this.calculateCardProb(cardIndex);

        //Calculate the importance based on probability that the card is a specific card
        float importance = 0;
        for (String card : cardProb.keySet()){
            importance += this.aiModel.getCardImportance().get(card) * cardProb.get(card);
        }
        return importance;
    }

    /**
     * Determine the play with the highest probability of success
     * @return The card index and the probability of success for the best play
     */
    private Pair<Integer, Float> bestPlay(){
        return bestHelper(0, (succSum, best) -> succSum > best);
    }

    /**
     * Determine the discard with the highest probability that the card is not needed at this time
     * @return The card index and the probability the card is not needed at this time
     */
    private Pair<Integer, Float> bestDiscard(){
        return bestHelper(1, (succSum, best) -> succSum < best);
    }

    /**
     * Help determine the best play/discard
     * @param defaultSuccess The default value for success (0 if play, 1 if discard)
     * @param condition The condition to check determining whether or not a card is better than the previous
     * @return The index of the card and probability of success/card not needed for play/discard
     */
    private Pair<Integer, Float> bestHelper(int defaultSuccess, BiPredicate<Float, Float> condition){
        float probOfSuccess = defaultSuccess;
        float importance = 1;
        int bestCard = 0;
        float probSuccessSum = 0;
        HashMap<String, Float> cardProb;

        //Check if the card can go on a play pile
        for (int i : this.aiModel.getHandConstraints().keySet()){
            probSuccessSum = 0;

            cardProb = this.calculateCardProb(i);
            for (String card : cardProb.keySet()){
                if (!this.model.getRainbowMode().equals("wild") || card.charAt(0) != 'm') {
                    probSuccessSum += (this.model.getScoreByColor(card.charAt(0)) ==
                                    Character.getNumericValue(card.charAt(1)) - 1) ? cardProb.get(card) : 0;
                } else {
                    //Check if tha rainbow wild card can be played successfully
                    boolean couldPlay = false;
                    for (char c : Card.getValidSuits()){
                        couldPlay |= (c != 'm' &&
                                this.model.getScoreByColor(c) == Character.getNumericValue(card.charAt(1) - 1));
                    }
                    probOfSuccess += couldPlay ? cardProb.get(card) : 0;
                }
            }

            //Check the given condition and change the best card if it's better than the previous best
            if (condition.test(probSuccessSum * this.calculateImportance(i), probOfSuccess * importance)){
                probOfSuccess = probSuccessSum;
                importance = this.calculateImportance(i);
                bestCard = i;
            }
        }
        return new Pair<Integer, Float>(bestCard, probOfSuccess);
    }

    /**
     * Determine a hint with relatively high importance containing new information to give
     * @return The hint to give
     */
    private JSONObject pickHint(){
        ArrayList<Pair<Player, Integer>> totalHandImportance = new ArrayList<>();
        HashMap<Integer, HashSet<String>> importanceMaps = new HashMap<Integer, HashSet<String>>();

        //Calculate the importance of each other player's hand and keep track of their unique cards
        for (Player p : this.model.getPlayers()){
            if (p != this.aiModel) {
                int importance = 0;
                for (int i = 0; i < p.getHand().getSize(); i++){
                    importance += this.aiModel.getCardImportance().get(p.getHand().getCard(i).toString());
                    importanceMaps.putIfAbsent(p.getPlayerId(), new HashSet<String>());
                    importanceMaps.get(p.getPlayerId()).add(p.getHand().getCard(i).toString());
                }
                totalHandImportance.add(new Pair<Player, Integer>(p, importance));
            }
        }

        //Sort the players by most important hand
        this.hintHelper(totalHandImportance);

        //Create a JSON object to be returned
        JSONObject hint = new JSONObject();
        hint.put("notice", "inform"); //Build with notice so it can be compared to recent hints
        hint.put("player", totalHandImportance.get(0).getKey().getPlayerId());
        hint.put("rank", 1);

        //Toggle for whether to hint about suit or rank
        boolean suit = true;

        do {
            //Remove the old information and insert information for player with most important hand
            this.hintHelper(totalHandImportance);
            hint.remove(suit ? "rank" : "suit");
            String bestCard = this.highestImportance(importanceMaps.get(hint.get("player")));
            hint.put(suit ? "suit" : "rank", suit ? bestCard.substring(0, 1) :
                    Character.getNumericValue(bestCard.charAt(1)));

            if (!suit){

                //If the suit and rank have been tried, no longer consider that card
                importanceMaps.get(hint.get("player")).remove(bestCard);

                //Re-sort the players based on importance after removing the card
                totalHandImportance.set(0,
                        new Pair<Player, Integer>(totalHandImportance.get(0).getKey(),
                                totalHandImportance.get(0).getValue() -
                                this.aiModel.getCardImportance().get(bestCard)));

                //If the player no longer has cards, remove that player from consideration
                if (importanceMaps.get(hint.get("player")).isEmpty()){
                    totalHandImportance.remove(0);
                }
            }
            suit = !suit;

            //Wait until a hint that gives new information to a player is made
        } while (this.aiModel.getRecentHints().containsKey(hint.get("player")) &&
                this.aiModel.getRecentHints().get(hint.get("player")).contains(hint));

        hint.remove("notice");
        hint.put("action", "inform");
        return hint;
    }

    /**
     * Help sort players by the importance of their hands
     * @param handImportance The arraylist of players and respective hand importance
     */
    private void hintHelper(ArrayList<Pair<Player, Integer>> handImportance){
        handImportance.sort(new Comparator<Pair<Player, Integer>>() {
            public int compare(Pair<Player, Integer> o1, Pair<Player, Integer> o2) {
                return o1.getValue() > o2.getValue() ? 1 : 0;
            }
        });
    }

    /**
     * Help find the highest importance card among a set of cards
     * @param cards The set of cards
     * @return The highest importance card
     */
    private String highestImportance(HashSet<String> cards){
        String best = "";
        int bestImportance = 0;
        for (String c : cards){
            if (this.aiModel.getCardImportance().get(c) > bestImportance){
                best = c;
                bestImportance = this.aiModel.getCardImportance().get(c);
            }
        }
        return best;
    }

    /**
     * Decide the move the AI will make
     * @return The JSON message for the move the AI will make
     */
    private JSONObject decideMove(){
        Pair<Integer, Float> bestMove = this.bestPlay();
        JSONObject result = new JSONObject();
        if (bestMove.getValue() >= this.PLAY_CUTOFF){
            result.put("action", "play");
            result.put("position", bestMove.getKey() + 1);
            return result;
        }

        bestMove = this.bestDiscard();
        if (bestMove.getValue() <= this.DISCARD_CUTOFF &&
                this.model.getHintToken().getHintToken() < this.model.getHintToken().MAX){
            result.put("action", "discard");
            result.put("position", bestMove.getKey() + 1);
            return result;
        }

        return this.model.getHintToken().getHintToken() == 0 ? result : this.pickHint();
    }

    /**
     * obatin the test message
     * @param msg the initial test message
     * @return the final test message as a string
     */
    private String testMessage(String msg){
        String result = msg + "\n";
        result += "Best Discard: " + this.bestDiscard() + "\n";
        result += "Best Play: " + this.bestPlay() + "\n";
        result += "Pick Hint: " + this.pickHint() + "\n";
        result += "Decide Move: " + this.decideMove() + "\n";
        return result;
    }

    public static void main(String[] args){
        AIModel aiModel = new AIModel(50, "none",
                new Hand(new String[]{null, null, null, null, null}, 5), 1);

        Model model = new Model();
        model.setGame(new Game(2, 10, "blah", "blah"));
        model.setRainbowMode("none");
        model.addPlayer(aiModel);
        model.addPlayer(new Player(new Hand(new String[]{"b1", "r1", "r3", "r5", "b2"}, 5), 2));

        AIController aiController = new AIController(model, aiModel);

        System.out.println(aiController.testMessage("Without any additional information"));



        JSONObject hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "b");
        hint.put("player", 2);

        aiModel.updateRecentHints(hint);

        System.out.println(aiController.testMessage("After p2 has received hint about suit b cards"));


        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 1);
        hint.put("player", 2);

        aiModel.updateRecentHints(hint);

        System.out.println(aiController.testMessage("After p2 has received hint about rank 1 cards"));



        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("suit", "y");
        hint.put("cards", new boolean[]{false, false, true, true, false});

        aiModel.receiveHint(hint);

        hint = new JSONObject();
        hint.put("notice", "inform");
        hint.put("rank", 1);
        hint.put("cards", new boolean[]{true, false, true, false, false});

        aiModel.receiveHint(hint);

        System.out.println(aiController.testMessage("After receiving hints: card 3, 4 are y; card 1, 3 are 1"));
    }
}
