package Startup;

import Controller.Controller;
import Model.Model;
import Network.Network;
import View.View;
import javafx.application.Application;
import javafx.stage.Stage;

public class Client extends Application {

    private static Network network = new Network();

    public static void main (String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Model model = new Model();
        Controller controller = new Controller(model);
        View view = new View(primaryStage, controller);
    }

    public static Network getNetwork () {
        return network;
    }
}
