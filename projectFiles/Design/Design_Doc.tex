\documentclass{article}
\usepackage{hyperref}
\usepackage{url}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\begin{document}

\title{Design}
\author{Group E2: alh577, chj482, chl857, fas216, fci588, swl567}
\date{March 2019}
\maketitle
\thispagestyle{empty}

\pagebreak
\tableofcontents
\thispagestyle{empty}
\pagebreak


\section{Architecture}
A hybridized approach has been decided for the architectural style.
This particular approach will be described as a Hybridized Model-View-Controller
with an Event driven system. The Controller will be implemented with an
Event driven approach, and the Model will be updated based on
previous events. This hybridized style was chosen because it demonstrates
3 key advantages: organization, natural event flow in the context of a game, and the separation of modules.

The first advantage of this style is Organization. Organization allows the Design
team to foresee all possible states and paths the game might execute: from Game
Creation, to waiting in lobby, game play and finally the end of the game. All possible
states are easily viewable for further design and construction stages. In the event that a state becomes required/redundant it’s
possible to easily and graphically represent these changes in the finite state diagram.

The second advantage of implementing a hybridized style of MVC and Event-Controller
is that the controller aspect of MVC is given more specificity with how information
flows/transforms/validated from the user to the model. The game Hanabi can be thought
of as a flow from one state to another, with defined beginnings and endings. As well,
since there is a time limit restriction on each game we know that the life span of
any such game is finite and we must take care in arranging the flow diagram to avoid trapping
the user in cycles. Each transition, or edge, in the cycle will represent an action that has been
performed by either the user or the server, with the model being updated accordingly.

The third advantage of a hybridized style is being able to implement a view
layer that is updated through each stage of game creation, and autonomously updated
after each save to the model layer during game play. This allows the user the feel
the flow of the game, and not have to worry about refreshing contents at the view
level

\section{Event State Diagram}
In the following state diagrams, vertices represent current states, and edges
represent a specific action taken to leave a current state and enter the next state.
Vertices with a 'V' represent start states and vertices enclosed in a circle indicate
end states.

\subsection{Pre-Game}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"../finite state diagrams/finite state 1".jpg}
\caption {Pre-Game Diagram}
\label{fig1}
\end{figure}

\subsubsection{NSID Prompt}
This is the start point for a human player to launch the client.
The player will need to enter a valid NSID, in a pop up prompt, with the format xxx\#\#\#, where x represents
letters, and \# represents numbers. This state will also save the NSID of the user to the model.

There are two transitions from this state. If the NSID was not in a correct format the state
will transition back to itself and prompt the user for an NSID. The second transition from
this state is if the NSID was valid, in this case the NSID will be saved to the model the user
will transition to the create/join prompt state.

\subsubsection{Create/Join Prompt}
This state will prompt the user using a pop up and two buttons for either create or join. Clicking on the create
button will progress the user down the create edge and the join button will progress the user down the create edge.

The create option will put the user down the path to set up a new game and the the join option will put the user down
the path to join a pre-existing game.

\subsubsection{Create Game Prompt}
This state will create a pop up prompt for the user with a drop down box with the number of players the user wishes to have
in the game and a slide bar that will indicate the timeout, in seconds, for the game to be created, and a continue button. Defaults
for the two options will be set at two for the number of players and 120 seconds for the timeout.

There is only one transition out from this state since the input is restricted. Once the user selects the continue button they will progress
to the Create Game state.

\subsubsection{Create Game}
In this state a message will be constructed with the previous NSID, number of players and timeout and sent to the server to create a game.
There are 3 possible transitions from this state. The first transition is that the server successfully created a game for the user in which
case the Game ID and Game secret that the server returns is saved to the model and is displayed to the user using a popup. The second possible
response from the server is that a game already exists under the NSID that was provided, in this case the server will return the Game ID and the Game
Secret of the game already in progress. The previous 2 transitions will progress the user to the Display Info state. The final transition is if the server
returns an error, in this case the user will progress to the exit prompt state.

\subsubsection{Display Info}
This state's job is to save and present the game info to the the model and user respectively. There are 2 possible transitions from this state depending on if
the game already exists. If a game was successfully created by the server then the user will progress to the Join Game state. If there was already a game in
session then the user will progress to the Exit Prompt state.

\subsubsection{Game Info Prompt}
This state will create a pop up with three elements. There will be 2 text fields that will be for game ID and game secret respectively, and then a continue button
that will be used for when the user want to submit the information in the fields. There can be no client side checking of the two text fields since only the server
will know if the game ID and game secret are a valid combination.

\subsubsection{AI}
This is the starting state for an AI player. This state will be started from the command line and be passed arguments for the game ID and a game secret.
The AI player will not receive prompts from the client since it will have no way of responding to the in the same way that a human player could.

There are 2 transitions from this state if the game id and the game secret were correctly passed in as arguments then the AI will progress to the Join game state.
If the game state and the game secret arguments were not filled in properly then the client will progress to the exit state.

\subsubsection{Join Game}
The Join class is where the client uses the game information passed in to join an already existing game. There are 4 transitions from this state.

The first transition from this state is that the client was able to successfully join a game in which case the client will progress to the the Game Lobby state that is outlined in the Game section and is
represented in Fig2.

The three other transitions are if the server sends back an error, a game full message or a no game message to the client. If any of the 3 previous messages are received from
the server the client will progress to the Exit Prompt state.

\subsubsection{Exit Prompt}
This state will prompt the user with a pop up with 2 elements: Yes, and No. There are 2 transitions from this state based on if the user selects Yes or No.
If the user selects No then they are sent to the Create/Join Prompt state. If the user selects Yes then they are sent to the Exit state.

\subsubsection{Exit}
This is a final state in which the client will close.

\subsection{Game}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"../finite state diagrams/finite state 2".jpg}
\caption {Game Diagram}
\label{fig2}
\end{figure}


\subsubsection{Game Lobby}
This state represents all the players gathering before a game begins. There are 3 transitions from this state.

The first transition is if the client gets a message from the server that the game is starting. This is done with the game starts message that the server sends as a notice.
The client will then progress to the save info state and save the info that the server has sent it in the game start notice.

The second transition from this state is if the client receives a message that the game has been cancelled this message will be received when the server exceeds the timeout times 10 when
waiting for players to join the game. The client will then progress to the game cancelled state.

The third transition from this state is if the client receives a notice from the server that a player has left the lobby. In this case the client will simply transition back to the
Game Lobby state.
\subsubsection{Save Info to Model}
In this state the client will save whatever information it has received from the notice that brought it to this state to the model. For instance when the client arrives
at this state from the Game Lobby state it will save the players hands to the model. There are six transitions from this state, though most of the transitions are back to the Save Info to Model state.

The first transition from this state is if the client receives a notice from the server that it is its turn, after receiving this notice the client will progress to the
Action Prompt state.

The second transition from this state is if the client receives a dicard notice from the server, in this case the the client will save the card that was discarded to its model as well as
update the new card that the player received and increment the player turn.

The third transition from this state is if the client receives a played notice from the server, in this case the client will save the card that was played to the model
and update the new card that the player received as well as increment the player turn.

The fourth transition from this state is if the client receives an inform notice from the server. There are 2 versions of this message that the server can send to the client.
The first version of this message is if the the client is the recipient of the hint in which case it will be informed about an aspect of its hand. If it is the second version
of this message the client will be told which player has been informed and what the hint was. As with the previous 2 transitions this transition will also update the player turn and
loop back to the Save Info to Model state.

The fifth transition from this state is if the client receives a game cancelled notice from the server. In this case the client will progress to the game cancelled state.

The sixth transition from this state is if the client receives a game end notice from the server. This notice means that the game has ended successfully and that the client can
progress to the game end state.
\subsubsection{Action Prompt}
This state will use a prompt to ask the user what kind of move they would like to make on their turn. The pop up should have 3 buttons which are labeled Play, Discard, and Hint.
This state will also check to see if a particular action is valid. This means that if there already exists 8 hint tokens the state should not display the Discard button since that
would be an invalid move.

There are 4 possible transitions from this state based on which button is pressed on the pop up. If the user selects Play they will be taken to the Play Prompt state. Discard to the
Discard Prompt state and Hint to the Info Prompt state. If the user does not select a move in the timeout period then the client will go to the game cancelled state.
\subsubsection{Play Prompt}
This state will use the view to get the user to select a card from their own hand. The only clicks that will have action on the view are those that correspond to cards in the user's hand.
There are 3 transitions from this state. The first two lead back to the Save Info to Model state and are the result of the action building the stack or burning a card.

The last transition is if the user takes too long to select a card in which case the client will progress to the Game Cancelled state.
\subsubsection{Discard Prompt}
This state will use the view to get the user to select a card from their own hand. The only clicks that will have action on the view are those that correspond to cards in the user's hand.
There are 2 transitions from this state. The first leads back to the Save Info to Model state so the client can record what action then have made on the model.

The second transition is if the user takes too long to select a card in which case the client will progress to the Game Cancelled state.
\subsubsection{Info Prompt}
This state will start by creating a pop up with 2 buttons corresponding to rank and suite. After a button is pushed the popup will close and then the user will be
required to select a card from another player's hand. The card that is selected will be a representative of the hint they selected in the pop up prompt.

For example if the user selects Rank in the pop up prompt and then selects player 2's Red 5 player 2 will get information about all the 5's in their hand.
If the user selected Suite in the pop up prompt and then selects player 2's Red 5  player 2 will get information about all the Red cards in their hand.

By using this series of prompts we can make sure that only valid hints are constructed and there will be no need to validate the hint once it is constructed.

There are two transitions from this state. The first transition is for a hint being sent to the server and the client will progress to the Save Info to Model.
The second transition is when a time out is encountered. in this case the client progresses to the game cancelled state.

\subsubsection{Game End}
This is a final state for the proper end to a game. In the case that the game ends on this state the score will be displayed and the client will exit.

\subsubsection{Game Cancelled}
This is a final state for when the game ends prematurely. The client will exit from this state.

\section{Class Diagram}
This is a list of the classes and their methods that will need to be implemented for this program.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"UML_diagrams/class_diagram_1".jpg}
\caption {UML Diagram}
\label{fig3}
\end{figure}

\subsection{Client}
The client class (fig3) will be the main class from which the program will be run. It
will be in charge of creating the model view and controller and making sure
that each is properly set up. This means the controller and view having a
reference to the model. As well, it will also set up the AI player and fill in part of the model on its behalf.

\subsubsection{Attributes}
\begin{itemize}
    \item model model
    \item controller controller
    \item view view
    \end{itemize}

\subsubsection{Constructor}
\begin{itemize}
    \item Controller(Model updated\_model)
    \end{itemize}


\subsubsection{Attributes}
\begin{itemize}
    \item public Model updated\_model
   \item private int invalid\_info
   \item private int invalid\_discard
   \item private int invalid\_play
   \item private boolean game\_cancelled
   \item private boolean game\_end
   \item private boolean game\_lobby
   \item private boolean save\_to\_lobby
   \item private boolean your\_turn
    \end{itemize}

\subsection{Controller}
The controller (fig3) class will keep a copy of the Model. The controller class
will be responsible for progressing the game through states, prompting Players
and updating data to the model, as described in Chapter 1.

\subsubsection{Constructor}
\begin{itemize}
    \item Controller(Model updated\_model)
    \end{itemize}


\subsubsection{Attributes}
\begin{itemize}
    \item public Model updated\_model
   \item private int invalid\_info
   \item private int invalid\_discard
   \item private int invalid\_play
   \item private boolean game\_cancelled
   \item private boolean game\_end
   \item private boolean game\_lobby
   \item private boolean save\_to\_lobby
   \item private boolean your\_turn
    \end{itemize}


\subsubsection{private void set\_NSID(String NSID)}
Will grab data for NSID, and set it into the attribute
provided NSID is of standard format.
\subsubsection{private void set\_number\_of\_players(int num\_players)}
Will take data from the number of players prompt and update the model with
a primed value for number of players.
\subsubsection{private void print\_players\_left()}
When update of players left is given by server update players with how many
more players need to join the game before game starts.
\subsubsection{private void prompt\_turn()}
When message for players turn has occurred, signal player turn to commence by
launching action prompt for the current players to use.
\subsubsection{private void launch\_game\_end()}
End of game scenarios have occurred, so automatic launch of end game displayed
will be activated.
\subsubsection{private void launch\_game\_cancelled()}
End game through timeout or 3x inaction in a row have occurred, gives notice
that game has come to an end.
\subsubsection{private void check\_play()}
Check for valid play and increment invalid play in the case that play is
invalid. Reset the counter back to zero if valid play occurred. This method will
also raise the game cancelled flag in the event that 3x invalid plays occurred.
\subsubsection{private void check\_discard()}
Check for valid discard and increment invalid discard in the case that play is
invalid. Reset the counter back to zero if valid discard occurred. This method will
also raise the game cancelled flag in the event that 3x invalid discard occurred.
\subsubsection{private void check\_info()}
Check for valid info and increment invalid play in the case that play is
invalid. Reset the counter back to zero if valid info occurred. This method will
also raise the game cancelled flag in the event that 3x invalid info occurred.
\subsubsection{private void build\_deck(Card acard)}
Updates the card stack based on acard, when a build message has occurred.
\subsubsection{private void update\_hand(Card acard)}
When a build or burn notice appears, the hand of the player who played will be updated by:
Removing the played card from the player's hand.
Updating the player's hand with a brand new drawn card.
\subsubsection{private void pick\_card()}
Retrieves the cards suit and rank after selection for further processing.

\subsection{Key\_Value\_Pair}
This class (fig3) contains 2 strings, one will be a key and the other a value. This
class will mostly be used as information transfer between the controller and
the message class which handles communication with the server.

\subsubsection{Constructor}
\begin{itemize}
    \item key\_value\_pair(string key, string value)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private string key
   \item private string value
    \end{itemize}


\subsubsection{public string get\_key()}
Returns the key.
\subsubsection{public string get\_value()}
Returns the value.
\subsubsection{public string to\_string()}
This method returns the key and value respectively, separated by a colon.
\subsubsection{public void set\_key(string key)}
Saves the passed in key as the classes private key.
\subsubsection{public void set\_value(string value)}
Saves the passed in value as the classes private value.
\subsubsection{public void set\_pair(string key, string value)}
Saves the key and value that were passed in as the classes private key and
value respectively.

\subsection{Network}
This is the class (fig3) that will be responsible for saving, setting up and tearing
down the network connections. It will need to know the host name of the server,
the port number it will be listening on, and the secret used to has the
messages. This also means that it will need to store the connection itself
which will either be a socket or a file descriptor. As well, this class should
be a singleton since there will only need to be one connection to the server.

\subsubsection{Constructor}
\begin{itemize}
    \item network(string host\_name, int port, string secret)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private string host\_name
   \item private int port
   \item private secret
   \item private connection (either a socket or a file descriptor)
    \end{itemize}


\subsubsection{public connection get\_connection()}
This will be a public method in which the connection can be accessed.
\subsubsection{public string get\_secret()}
This will be a public method in which the secret can be accessed.

\subsection{Message}
The message class (fig3) will contain a JSON object and can be created by either
passing it a list of key value pairs or by using its recv function to get the
JSON object from a server message. The Message function will also have to
collaborate with the time function in order to add a timestamp as per the JSON
message format. As well the message class will have to know about the secret
hint that it will use to hash its messages as well as validate messages from
the server, the secret should be recorded in the network class. This means that
the message class will need a reference to the network object in order to get
access to the secret for the has as well as the connection to send the message.

\subsubsection{Constructor}
\begin{itemize}
    \item message(network object)
   \item message(network object, list <key\_value\_pair > info)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private network object
   \item private string secret
   \item private JSON obj payload
    \end{itemize}


\subsubsection{private void create\_hash()}
This method adds an MD5hash to the payload by applying a MD5hash to the JSON message
that includes a dummy hash, which is the secret that was passed in with the
constructor.
\subsubsection{private void create\_timestamp()}
This method uses the time function in java to get the time since the epoch and
adds that information to the payload.
\subsubsection{private boolean check()}
This method will check the saved payload against the same payload that has the
md5hash value replaced with the secret. Returns True if the payload is the same
as the hashed JSON obj with the secret, False otherwise.
\subsubsection{public void Send()}
This message makes sure that the payload is ready to go using the previous 2
methods to fill out the payload as needed. It will then send the payload off to
the server.
\subsubsection{public void recv()}
This method will wait for a message from the server and save it as the payload.
It will also use the private check function to check if the server message is
legitimate.
\subsubsection{list <key\_value\_pair > get\_message()}
This function will return the payload, minus the timestamp and the MD5hash, as
a list of key value pairs.
\subsubsection{public void create\_message(list <key\_value\_pair > info)}
This method will fill out the payload with the infor included in the list of
key value pairs. Timestamp and Md5hash are not expected in this list.

\subsection{View}
This class (fig3) facilitates the use of data to and from the model and displays it
in a graphical user interface. It will also need to hook up elements of the .xml file to their components in the model.

\subsubsection{Constructor}
\begin{itemize}
    \item view(Model model)
   \item view(Model model, num\_players)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private Model model
   \item private int num\_players
   \item private final String FXML\_LOCATION
    \end{itemize}


\subsubsection{public javafx.scene.Scene create\_window()}
This method will create a game window based on the number of players in the game
it will use the appropriate fxml file from FXML\_LOCATION and return a Scene object.
\subsubsection{public void show\_window(Scene scene)}
This method will set up a javafx.stage.Stage with the given scene and display it
onto the screen.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"UML_diagrams/class_diagram_4".jpg}
\caption {UML Diagram}
\label{fig4}
\end{figure}

\subsection{Player}
The Player (fig4) stores information about a player.

\subsubsection{Constructor}
\begin{itemize}
    \item Player(Hand hand)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private Hand hand
    \end{itemize}

\subsubsection{void set\_hand(Hand hand)}
set a player's hand
\subsubsection{Hand get\_hand(Hand hand)}
Get the info of a player's hand
\subsubsection{string to\_string()}
This returns the hand information as a string

\subsection{AIPlayer extends Player}
The AIPlayer (fig4) will use information stored in the model and itself to decide
what actions the AI should take.

\subsubsection{Constructor}
\begin{itemize}
    \item AiPlayer(int cards\_in\_game)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private int cards\_in\_game
   \item private HashMap <Card, int > card\_importance
   \item private HashMap <int, HashMap <Car, int > > hand\_constraints
   \item private HashMap <int, Hint[] > recent\_hints
   \item private HashMap <Card, int > discarded\_cards
    \end{itemize}


\subsubsection{private int total\_cards\_unseen()}
Calculates the number of cards that the AI can not see (in hand and in draw
pile).
\subsubsection{private int num\_card\_left(Card c)}
Calculates the number of cards matching Card c that have not been seen.
\subsubsection{HashMap <Card, Float > calculate\_card\_prob(int cardIndex)}
Calculates the probabilities that the card in the given position is a certain
suit and rank (for all suits and ranks it could be).
\subsubsection{private float calculate\_importance(int cardIndex)}
Determine the importance of a card in hand.
\subsubsection{Pair(int, float) best\_play(HashMap <int, HashMap <Card ,Float > > handProbs)}
Determine the best card to play and the probability that the action would be
successful.
\subsubsection{Pair(int, float) best\_discard(HashMap <int, HashMap <Card, Float > > handProbs)}
Determine the best card to discard and the probability weighted with importance
that the card would not be needed.
\subsubsection{private Pair(Hint, int) pick\_hint()}
Pick a hint to give that would provide new information and give (not necessarily
new) information with relatively high importance. Include the importance in
return.
\subsubsection{public Move decide\_move()}
Determine the move the AI should make accounting for importance/probability of
success.
\subsubsection{public void receive\_hint(Hint h)}
Interpret the hint and update the constraints for affected cards in hand.
\subsubsection{public void updateHints(Hint h, int player)}
Update the recent hints for that player.
\subsubsection{public void player\_drew(int player, Card c)}
Remove from recent hints the hints that would now give new information to the
player.
\subsubsection{public void update\_discarded(Card c)}
Add the discarded card to the Hashmap of discarded cards.

\subsection{Card}
This class (fig4) is used to create the Card objects for the Hanabi game.
This class contains the fields for a card's suit and number, as well as setters
and accessors to set and obtain the card's information.

\subsubsection{Constructor}
\begin{itemize}
    \item Card(char number, char suit)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private char number
   \item private char suit
    \end{itemize}


\subsubsection{public void set\_suit(char suit)}
This sets the suit.
\subsubsection{public void set\_number(char suit)}
This sets the number.
\subsubsection{public char get\_suit()}
This returns the suit as a character.
\subsubsection{public char get\_number()}
This returns the number as a character.
\subsubsection{public boolean check\_suit(char suit)}
This checks for valid suit and returns a boolean.
\subsubsection{public boolean check\_number(char number)}
This checks for valid number and returns a boolean.
\subsubsection{public boolean check\_card(char number, char suit)}
This checks for valid cards and numbers and returns a boolean.
\subsubsection{public String to\_string()}
This returns the card information as a string.

\subsection{Hint}
This class (fig4) will store a hint and a methods to access,
check and get hint.

\subsubsection{Constructor}
\begin{itemize}
    \item Hint()
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private String hint
    \end{itemize}


\subsubsection{public void set\_hint(String hint)}
This sets the hint and put it into the hint field.
\subsubsection{public String get\_hint()}
This obtains the hint and returns a string.
\subsubsection{public boolean check\_hint()}
Check if the hint is a valid hint and return a boolean.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"UML_diagrams/class_diagram_2".jpg}
\caption {UML Diagram}
\label{fig5}
\end{figure}

\subsection{Hint\_Token}
This class (fig5) will contain the number of hint tokens and check methods
for how many hint tokens are still available, as well as the modification
methods to change the number of hint tokens.

\subsubsection{Constructor}
\begin{itemize}
    \item hint\_token(int number\_of\_hints)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private int number\_of\_hints
    \end{itemize}

\subsubsection{public void increase\_hint\_token()}
This method increases the number of hint tokens by 1 if the number of fuse tokens has not reached
the maximum number of hint tokens.
\subsubsection{public void decrease\_hint\_token()}
This method decreases the number of hint tokens by 1 if the number of hint will not be lower than
the minimum number of hint tokens.
\subsubsection{public void set\_hint\_token(int hint\_number)}
Set the number of hint tokens to a valid number.
\subsubsection{public int get\_hint\_number()}
Return the current amount of hint tokens.
\subsubsection{public boolean check\_hints(int pseudo\_hint\_number)}
Check if the parameter is a valid number for hints. Returns a boolean value for valid or invalid.

\subsection{Fuse\_Token}
This class (fig5), will contain the number of fuse tokens and check methods
for how many fuse tokens are still available, as well as the modification
methods to change the number of fuse tokens.

\subsubsection{Constructor}
\begin{itemize}
    \item fuse\_token(int number\_of\_fuses)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private int number\_of\_fuses
    \end{itemize}


\subsubsection{public void increase\_fuse\_token()}
This method increases the number of fuse tokens by 1 if the number of fuse tokens has not reached
the maximum number of fuse tokens.
\subsubsection{public void decrease\_fuse\_token()}
This method decreases the number of fuse tokens by 1 if the number of fuse will not be lower than
the minimum number of fuse tokens.
\subsubsection{public void set\_fuse\_token(int fuse\_number)}
Set the number of fuse tokens to a valid number.
\subsubsection{public int get\_fuse\_number()}
Return the current amount of fuse tokens.
\subsubsection{public boolean check\_fuses(int pseudo\_fuse\_number)}
Check if the parameter is a valid number for fuses. Returns a boolean value for valid or invalid.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"UML_diagrams/class_diagram_3".jpg}
\caption {UML Diagram}
\label{fig6}
\end{figure}

\subsection{Hand}
This class (fig6) represents a players hand. It will have a list of cards, methods
to set and get new cards.

\subsubsection{Constructor}
\begin{itemize}
    \item Hand()
   \item Hand(Hashtable <int, Card >)
   \item Hand(Hashtable <int, Card >, int number\_of\_cards)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private Hashtable <int, Card > Cards
   \item private int number\_of\_cards
    \end{itemize}


\subsubsection{public void set\_hand()}
This sets up the hand object with the number of cards.
\subsubsection{public Hashtable <int, Card > get\_hand()}
This returns the hand as a hashtable of cards with integer indexes.
\subsubsection{public Card get\_card(int key)}
This method returns the card by the cards key value.
\subsubsection{public void put\_card(Card new\_card)}
This method puts a new card into the player's hand.
\subsubsection{public int count\_cards\_by\_suit(char suit)}
This method counts cards by certain suits and returns an integer.
\subsubsection{public int count\_cards\_by\_number(char number)}
This method counts cards by certain numbers and returns an integer.
\subsubsection{public int count\_cards\_by\_Card(Card card)}
This method count\_cards by certain cards and returns an integer.
\subsubsection{public boolean is\_empty()}
This method checks if the hand is empty and returns a boolean.
\subsubsection{public boolean is\_full()}
This method checks if the hand is full and returns a boolean.
\subsubsection{public String to\_string()}
This method returns information of the hand as a string.


\subsection{Game}
This class (fig6) stores, sets and obtains information regarding the game such as
the game ID, game token, number of players, time\_period and player turns.

\subsubsection{Constructor}
\begin{itemize}
    \item Game(int number\_of\_players, int time\_out\_period, String game\_id, String game\_token)
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
    \item private int number\_of\_players
   \item private int time\_out\_period
   \item private int player\_turn
   \item private String game\_id
   \item private String game\_token
    \end{itemize}


\subsubsection{public void set\_number\_of\_players(int number)}
This sets the number of players in the game.
\subsubsection{public int get\_number\_of\_players()}
This returns the number of players in the game.
\subsubsection{public void set\_time\_out\_period(int time\_out\_period)}
This sets the time-out period.
\subsubsection{public int get\_time\_out\_period()}
This returns the time-out period as a integer.
\subsubsection{public void set\_player\_turn(int turn)}
This method sets the turns for the players.
\subsubsection{public int get\_player\_turn() }
This returns the player's turn as an integer.
\subsubsection{public void set\_game\_id(String id)}
This sets the game ID period.
\subsubsection{public String get\_game\_id()}
This returns the game ID as a String.
\subsubsection{public void set\_game\_token(String game\_token)}
This sets the game token.
\subsubsection{public String get\_game\_token()}
This returns the game token as a integer.
\subsubsection{public boolean check\_game\_token(String game\_token)}
This checks if the game token is valid and returns a boolean.

\subsection{Model}
This class (fig5) is the model component of the model-view-controller
architecture. It contains a game object that stores number of players
and their hands as well as the player's turn. This class also contains
a discard pile, a play card pile, and a card-ready-to-be-played pile.
This model also has a stack of hints with the most recent hints on the
top of the stack.

\subsubsection{Constructor}
\begin{itemize}
    \item Model()
    \end{itemize}

\subsubsection{Attributes}
\begin{itemize}
\item private String NSID
\item private View view
\item private Game game
\item private Stack <Hint > hints
\item private Stack <Card > discard\_pile
\item private Stack <Card > play\_card\_pile
\item private Stack <Card > ready\_pile
\item private Hashtable <int, Hand > hands
\item private Hint\_token hint\_token
\item private Fuse\_token fuse\_token
    \end{itemize}


\subsubsection{public void set\_NSID(String nsid)}
This sets the NSID.
\subsubsection{public String get\_NSID()}
This returns the NSID as a string.
\subsubsection{public boolean check\_NSID(String nsid)}
This checks if the NSID is valid and returns a boolean.
\subsubsection{public void update\_View()}
This updates the view object.
\subsubsection{void create\_game(int number\_of\_players, int time\_out, String game\_id, String token)}
This initializes the game object.
\subsubsection{public Game get\_game()}
This obtains the game object.
\subsubsection{public void put\_hints(String hint)}
This puts a new hint into the hint stack if the hint is valid.
\subsubsection{public void put\_discard\_pile(Card card)}
This puts a new discarded card into the discard pile.
\subsubsection{public Card get\_top\_of\_dicard\_pile()}
This returns a copy of the top of the discard pile.
\subsubsection{public Card get\_top\_of\_play\_pile()}
This returns a copy of the top of the play card pile.
\subsubsection{public void put\_play\_pile(Card card)}
This puts a new played card into the play card pile.
\subsubsection{public Card pop\_ready\_pile()}
This pops the top of the cards that are ready to be played.
\subsubsection{public void discard\_from\_hand\_by\_key(int key)}
This discards a card from a hand by the hand's key.
\subsubsection{void new\_card\_in\_hand\_by\_key(int key, Card new\_card)}
This puts a new card in the hand specified by the given key.
\subsubsection{public void create\_hint\_token(int number\_of\_hints)}
This method creates a hint token object for the model.
\subsubsection{public Hint\_token get\_hint\_token()}
This returns the hint\_token object.
\subsubsection{public void create\_fuse\_token(int number\_of\_fuses)}
This creates the fuse token object.
\subsubsection{public Fuse\_token get\_fuse\_token()}
This returns the fuse token object.

\section{Appendix}
One of the biggest changes from the requirements document is that we have decided to proceed with a graphical user interface (GUI)
instead of a text based interface. Besides being a representation of the model to the user the GUI will also be important for the controller
as it offers interaction like clicking on a card for instance.

Another change from the requirements document is that the JSON API has been released. This communication protocol made it clear that
there were misunderstandings in the requirements as it pertained to canceling a game. It would seem, from the API that there
is no explicit message a client can send to cancel a game that is already in progress. The only information a game creator will
receive is that a game under their NSID already exists along with the game ID and the secret. It would seem that the game creator
will just have to wait for the life of the game to be over naturally since each game is finite in length due to the timeout implemented
during the creation of the game.

Additionally, the AI has changed slightly from the requirements document. After further investigation and the release of the JSON protocol it was determined that
calculating the statistics for other players's cards was not
feasible without performing calculations within each client, making such a feature cumbersome and not important. It was decided that an itermediate step
not thought of in the requirements document would be used to increase the quality of hints given. Rather than give random hints, the recent hints (those
that would give no new information) will be kept and the AI will choose a non-recent relatively high importance hint.


\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{"UML_diagrams/class_diagram".jpg}
\caption {UML Diagram}
\label{fig7}
\end{figure}

\end{document}
