package Model;

public class HintToken {

    public final int MAX = 8;
    private int numberOfHints;

    /**
     * Constructor for the HintToken class which initially sets the number
     * of hints to the maximum hint number
     */
    HintToken () {
        this.numberOfHints = MAX;
    }

    /**
     * Recovers the hint token number to the maximum
     */
    public void recoverHintToken () {
        this.numberOfHints = MAX;
    }

    /**
     * Increase the number of hint tokens by 1
     */
    public void increaseHintToken () {
        if (this.numberOfHints < MAX) {
            this.numberOfHints += 1;
        }
    }

    /**
     * Decreasing the number of hint tokens by 1
     */
    public void decreaseHintToken () {
        if (this.numberOfHints > 0) {
            this.numberOfHints -= 1;
        }
    }

    /**
     * Obtain the number of hint tokens
     * @return number of hint tokens as an integer
     */
    public int getHintToken () {
        return this.numberOfHints;
    }


    public static void main (String [] args) {

        HintToken testToken = new HintToken();

        if (testToken.getHintToken() != 8) {
            System.out.println("Fail to initialize the hint token number");
        }


        testToken.increaseHintToken();
        if (testToken.getHintToken() > 8) {
            System.out.println("The hint token exceeded the max value");
        }

        testToken.decreaseHintToken();
        if (testToken.getHintToken() != 7) {
            System.out.println("The hint token does not deacrese to 7 when it was at 8");
        }

        testToken.recoverHintToken();
        if (testToken.getHintToken() != 8) {
            System.out.println("The recover hint token method does not work as intended.");
        }

        //Test if the hint token number stays at 0 when the decreaseHintToken() method is called for over 8 times
        for (int i = 15; i > 0; i--) {
            testToken.decreaseHintToken();
        }

        if (testToken.getHintToken() != 0) {
            System.out.println("The hint token exceeded the minimum number when it shouldn`t have");
        }

        System.out.println("HintToken Test Completed");
    }

}
