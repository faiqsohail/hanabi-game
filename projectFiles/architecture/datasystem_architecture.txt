Datasystem Based Architecture

	Database:
		Composed of the datastore (the database) and components query the database
		to view any and all changes made.

		Pros:
			* Data Integrity
			* Scalability and Reusability
			* Reduces Overhead of Transferring Data
		Cons:
			* Data Duplication (accidental)
			* Latency for Network Based Datastore
			* High Dependency between datastore and the store accessors

	Blackboard:
		Acts as a central data repository where different components act independently
		on the data in the blackboard. 
		
		Pros:
			* Scalability
			* Concurrency
		Cons:
			* Synchronization Issues (when multiple components try changing the blackboard)
			* Hard to Test
			* Hard to Design

Sources:
https://www.tutorialspoint.com/software_architecture_design/data_centered_architecture.htm
