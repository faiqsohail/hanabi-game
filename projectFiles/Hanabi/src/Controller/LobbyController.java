package Controller;

import Model.Model;
import Network.Message;
import Network.Network;
import Startup.Client;
import View.View;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.json.simple.JSONArray;

import java.net.URL;
import java.util.ResourceBundle;

public class LobbyController implements Initializable {

    @FXML
    private Label neededLabel;

    @FXML
    private Label gameIdLabel;

    @FXML
    private Label gameTokenLabel;


    private Model model;
    private Thread thread;

    public LobbyController (Model model) {
        this.model = model;
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        neededLabel.setText("Waiting for players to join...");

        gameIdLabel.setText(model.getGame().getGameID());
        gameTokenLabel.setText(model.getGame().getGameToken());
        thread = new Thread(new LobbyUpdater());
        thread.start();
    }

    /**
     * Set the main label notifying the user of how many players are required to start the game
     * @param needed int - number of players needed
     */
    public void setNeededLabel (int needed) {
        Platform.runLater(() -> {
            neededLabel.setText("Waiting for " + needed + " more " + (needed > 1 ? "players" : "player") + " to join...");
        });
    }

    private class LobbyUpdater implements Runnable {

        @Override
        public void run() {
            while(true) {
                Message message = new Message();
                message.recv(Client.getNetwork().getInStream());

                if(message.getPayload().containsKey("notice")) {
                    if(message.getPayload().get("notice").equals("game starts")) {

                        JSONArray array = (JSONArray)message.getPayload().get("hands");
                        model.getGame().setInit_hand(array);
                        model.getGame().setNumberOfPlayers(array.size());
                        model.setRainbowMode(model.getRainbow());

                        Platform.runLater(() -> {
                            Stage stage = new Stage();
                            stage.setScene((View.createWindow(model.getGame().getNumberOfPlayers())));
                            stage.setTitle("Hanabi Game");
                            stage.centerOnScreen();
                            stage.show();

                            neededLabel.getScene().getWindow().hide();
                        });

                        break;


                    } else if (message.getPayload().get("notice").equals("game cancelled")) {
                        Platform.runLater(() -> {
                            Stage stage = new Stage();
                            Object controller = Controller.getController("InitialWindowController");
                            Parent root = View.loadXML("IntialWindow", controller);
                            stage.setScene(new Scene(root));
                            stage.setResizable(false);
                            stage.setTitle("Hanabi");
                            stage.centerOnScreen();
                            stage.show();

                            View.showAlert(Alert.AlertType.INFORMATION, "Game Cancelled",
                                    "The game has been cancelled!");

                            neededLabel.getScene().getWindow().hide();
                        });
                        break;
                    }
                }
            }
            thread.stop();

        }
    }
}
