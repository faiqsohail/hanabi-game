package Network;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.nio.Buffer;
import java.time.Instant;

public class Message {

    protected JSONObject payload;

    /**
     * Creates a message class just a deafult constructor
     */
    public Message () {

        payload = new JSONObject();
    }

    /**
     * Constuctor to build the message from an input source
     * @param in the in stream iof the buffer reader
     */
    public Message (BufferedReader in) {
        char inChar = '\0';
        String inString = "";
        JSONParser parser = new JSONParser();
        try {
            while(inChar != '}'){

                inChar = (char) in.read();
                inString = inString+inChar;
            }

            payload = (JSONObject) parser.parse(inString);

        } catch (ParseException e){
            System.err.print(e);
        } catch (IOException e){
            System.err.print(e);
        }

    }

    /**
     * Adds a property to the JSONObj bassed on the key and the value
     * @param key the key
     * @param value the value
     */
    public void addString (String key, String value) {

        payload.put(key,value);
    }

    /**
     * Returns a string representation of the JSON obj including {} and ""
     * @return the string information of the class
     */
    public String toString () {

        return payload.toJSONString();
    }

    /**
     * Returns the JSONObj itself
     * @return the payload
     */
    public JSONObject getPayload () {

        return payload;
    }

    /**
     * Sets a timestamp on the message bassed on seconds since the epoch
     */
    protected void setTimestamp () {

        payload.put("timestamp", Instant.now().getEpochSecond());

    }

    /**
     * Sends the payload out on the specified DataStream
     * @param out the out stream of the network
     */
    public void send (PrintStream out) {
        setTimestamp();
        out.print(toString());
    }

    /**
     * Empties the payload and rebuilds it from the JSON
     * obj in the Buffered Reader.
     * @param in the in stream of the buffer reader
     */
    public void recv (BufferedReader in) {
        char inChar = '\0';
        String inString = "";
        JSONParser parser = new JSONParser();

        payload.clear();

        try {
            while(inChar != '}'){

                inChar = (char) in.read();
                inString = inString+inChar;
            }

            payload = (JSONObject) parser.parse(inString);

        } catch (ParseException e){
            System.err.print(e);
        } catch (IOException e){
            System.err.print(e);
        }

    }


    public static void main (String[] argv) {

        Message testMessage = new Message();
        testMessage.addString("cmd","Hello world");

        //testing toString method
        if (!testMessage.toString().equals("{\"cmd\":\"Hello world\"}")){
            System.out.println(testMessage.toString());
            System.out.println("Error with addString");
        }

        //testing the getPayload method
        if (!((testMessage.getPayload().containsKey("cmd"))  &&
                testMessage.getPayload().containsValue("Hello world"))) {

            System.out.println("Error with getPayload");
        }

        //Test Sending a message
        try{
            FileOutputStream fileOut = new FileOutputStream("TCPConnection");
            PrintStream dataOut = new PrintStream(fileOut);

            testMessage.send(dataOut);

        } catch (IOException e){
            System.err.println(e);
        }

        //testing receiving a message
        try{

            FileReader fileReader = new FileReader("TCPConnection");
            BufferedReader buffReader = new BufferedReader(fileReader);

            Message testRecv = new Message(buffReader);

            //test if they match
            if(!(testMessage.toString().equals(testRecv.toString()))){
                System.out.println(testMessage.toString() + "=/=" + testRecv.toString());
            }

        } catch (IOException e){
            System.err.println(e);
        }
    }
}
