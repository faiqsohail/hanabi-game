package Network;

import org.json.simple.JSONObject;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class Action extends Message {

    protected Map obj;

    /**
     * Builds an action for play and discard.
     * @param action the action
     * @param position the player's position
     */
    public Action (String action, int position) {

        obj = new LinkedHashMap();

        obj.put("action",action);
        obj.put("position",position);

        payload = new JSONObject(obj);

    }

    /**
     * Creates a play or inform action
     * @param action the action
     * @param position the player's position
     * @param suit the suit
     */
    public Action (String action, int position, String suit) {

        obj = new LinkedHashMap();

        obj.put("action",action);

        // we need to differentiate between the variant play and the
        if(action.equals("play")) {
            obj.put("position", position);
            obj.put("firework", suit);
        }
        else if(action.equals("inform")) {
            obj.put("player", position);
            obj.put("suit", suit);
        }
        else {
            throw new RuntimeException ("Incorrect Action being built");
        }

        payload = new JSONObject(obj);
    }

    /**
     * Build the action for inform rank
     * @param action the action
     * @param position the positon
     * @param rank the rank
     */
    public Action (String action, int position, int rank){

        obj = new LinkedHashMap();

        obj.put("action", action);
        obj.put("player", position);
        obj.put("rank", rank);

        payload = new JSONObject(obj);
    }

    /**
     * override of too string to maintain JSON obj order
     * @return string
     */
    @Override
    public String toString () {

        obj.put("timestamp", payload.get("timestamp"));
        return payload.toJSONString(obj);
    }

    public static void main (String argv[]) {

        //setting up faux tcp connection
        FileOutputStream fileOut = null;
        FileReader fileReader = null;
        PrintStream dataOut = null;
        BufferedReader buffReader = null;

        try {
            fileOut = new FileOutputStream("TCPConnection");
            dataOut = new PrintStream(fileOut);

            fileReader = new FileReader("TCPConnection");
            buffReader = new BufferedReader(fileReader);
        } catch (IOException e) {
            System.out.println("Error opening faux streams");
        }

        Message recv = new Message();

        // test play normal
        Action testPlay = new Action ("play",1);
        testPlay.send(dataOut);
        recv.recv(buffReader);
        if (!recv.getPayload().get("action").equals("play")) {
            System.out.println("Error with play action");
        }
        if (Integer.parseInt(recv.getPayload().get("position").toString()) != 1) {
            System.out.println("Error with play position");
        }

        //test play variant
        Action testPlayVar = new Action ("play",1, "r");
        testPlayVar.send(dataOut);
        recv.recv(buffReader);
        if (!recv.getPayload().get("action").equals("play")) {
            System.out.println("Error with play action");
        }
        if (Integer.parseInt(recv.getPayload().get("position").toString()) != 1) {
            System.out.println("Error with play position");
        }
        if (!recv.getPayload().get("firework").equals("r")) {
            System.out.println("Error with play firework");
        }

        //test Discard
        Action testDisco = new Action ("discard", 1);
        testDisco.send(dataOut);
        recv.recv(buffReader);
        if (!recv.getPayload().get("action").equals("discard") ) {
            System.out.println("Error with discard action");
        }
        if (Integer.parseInt(recv.getPayload().get("position").toString()) != 1) {
            System.out.println("Error with discard position");
        }

        //test inform rank
        Action testInformRank = new Action ("inform",1, 1);
        testInformRank.send(dataOut);
        recv.recv(buffReader);
        if (!recv.getPayload().get("action").equals("inform") ) {
            System.out.println("Error with inform rank action");
        }
        if (Integer.parseInt(recv.getPayload().get("player").toString()) != 1) {
            System.out.println("Error with inform rank position");
        }
        if (Integer.parseInt(recv.getPayload().get("rank").toString()) != 1) {
            System.out.println("Error with inform rank rank");
        }
        //test inform suit

        Action testInformSuit = new Action ("inform",1, "r");
        testInformSuit.send(dataOut);
        recv.recv(buffReader);
        if (!recv.getPayload().get("action").equals("inform") ) {
            System.out.println("Error with inform suit action");
        }
        if (Integer.parseInt(recv.getPayload().get("player").toString()) != 1) {
            System.out.println("Error with inform suit position");
        }
        if (!recv.getPayload().get("suit").toString().equals("r")) {
            System.out.println("Error with inform suit suit");
        }
    }

}


