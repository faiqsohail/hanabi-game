package Model;

import java.time.Year;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Card {

    private final int rank;
    private final char suit;
    private static final char[] validSuits = new char[]{'b', 'g', 'm', 'r', 'w', 'y'};
    private static final int[] validRanks = new int[]{1, 2, 3, 4, 5};

    /**
     * Constructor for the Card class takes rank as an int and suit as a char
     * and saves them to the private variables within the class
     * @param rank the rank
     * @param suit the suit
     */
    public Card(int rank, char suit){
        if (!isValidCard(suit, rank)){
            throw new RuntimeException("Attempted to make invalid card");
        }
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * Create a Card from a string in the format "[suit][rank]" as in "r3"
     * @param details The string of the cards details
     */
    public Card(String details){
        char s = details.charAt(0);
        int r = Character.getNumericValue(details.charAt(1));
        if (!isValidCard(s, r)){
            throw new RuntimeException("Attempted to make invalid card");
        }
        this.suit = s;
        this.rank = r;
    }

    /**
     * Determine if a suit and rank are valid
     * @param suit The suit to check
     * @param rank The rank to check
     * @return Whether it is a valid card or not
     */
    private boolean isValidCard(char suit, int rank){
        boolean result = false;

        //Check for a valie rank
        for (int r : this.validRanks){
            result = result || (rank == r);
        }
        if (!result){
            return result;
        }

        //Check for a valid suit
        result = false;
        for (char s : this.validSuits){
            result = result || (suit == s);
        }
        return result;
    }

    /**
     * Get the rank variable
     * @return the rank as an int
     */
    public int getRank() {
        return rank;
    }

    /**
     * Get the suit variable
     * @return the suit as a character
     */
    public char getSuit() {
        return suit;
    }

    /**
     * return a string representation of the class
     * @return the string representation of the class
     */
    public String toString(){
        return (this.suit + Integer.toString(this.rank));
    }

    /**
     * checks the rank of the class
     * @param rank the rank of the card as an integer
     * @return a boolean check for valid rank
     */
    public boolean checkRank(int rank){
        return this.rank == rank;
    }

    /**
     * Check the suit of the class
     * @param suit the suit of the card as a char
     * @return a boolean check for valid suit
     */
    public boolean checkSuit(char suit){
        return this.suit == suit;
    }

    /**
     * Checks both the rank and the suit of the class
     * both must be true.
     * @param suit the card's suit
     * @param rank the card's rank
     * @return boolean checking for valid cards
     */
    public boolean checkCard(char suit, int rank){
        return checkSuit(suit) && checkRank(rank);
    }

    /**
     * This function returns the valid suits
     * @return a array with all the valid suits
     */
    public static char[] getValidSuits(){
        return validSuits;
    }

    /**
     * This function returns the valid ranks
     * @return a array with all the valid ranks
     */
    public static int[] getValidRanks(){
        return validRanks;
    }

    public static void main (String[] argv){

        Card testCard = new Card(5,'b');

        //getter/constuctor tests
        if (testCard.getRank() != 5){
            System.out.println("Error with getting rank or constructing rank");
        }

        if (testCard.getSuit() != 'b'){
            System.out.println("Error with getting suit or constructing rank");
        }

        //testing toString
        if (!testCard.toString().equals("b5")){
            System.out.println("Error with toString");
        }

        //testing checkRank
        if (!testCard.checkRank(5)){
            System.out.println("Error with positive checkRank");
        }

        if (testCard.checkRank(4)){
            System.out.println("Error with negative checkRank");
        }

        //testing checkSuit
        if (!testCard.checkSuit('b')){
            System.out.println("Error with positive checkSuit");
        }

        if (testCard.checkSuit('r')){
            System.out.println("Error with negative checkSuit");
        }

        //testing check card
        if (!testCard.checkCard('b',5)){
            System.out.println("Error with checkCard");
        }

        if (testCard.checkCard('b',1)){
            System.out.println("Error with checkCard");
        }

        if (testCard.checkCard('y',5)){
            System.out.println("Error with checkCard");
        }

        if (testCard.checkCard('r',1)){
            System.out.println("Error with checkCard");
        }

        //Test constructor logic
        try {
            testCard = new Card(-1, 'y');
            testCard = new Card(1, 'u');
            System.out.println("Error, constructor1 did not throw an exception when invalid card was made");
        } catch (RuntimeException e){}

        try {
            testCard = new Card("y5");
        } catch (RuntimeException e) {
            System.out.println("Error, constructor1 threw an exception when a vaild card was made");
            }

        try {
            testCard = new Card("u5");
            testCard = new Card("y6");
            System.out.println("Error, constructor1 did not throw an exception when invalid card was made");
        } catch (RuntimeException e) {}

        //testing getValidRanks
        if (getValidRanks() != validRanks){
            System.out.println("Error in getValidRanks");
        }

        //testing getValidSuits
        if (getValidSuits() != validSuits){
            System.out.println("Error in getValidSuits");
        }
    }
}
