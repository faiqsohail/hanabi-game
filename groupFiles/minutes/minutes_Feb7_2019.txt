Minutes (4) Thurs Feb 7 2019
Begin 1:27
Attendance:
Faiq
Frank
Alec
Paul
Chen
Shaun

Jenna started off the meeting by asking the team how handing in the
requirments doc had gone for us. Shaun had responded by saying it went
well but we may have touched a little to far onto the design aspect of
the course. Jenna then reminded the team that we are marked according
to the rubric so if we have any misunderstandings we should consult with
the rubric.

Shaun had told Faiq good job on the artwork for the cards, they look great.
He also suggested the Faiq should do gui mockups when he has free time.
Faiq agreed and said he would work on it.

Shaun asked the team which architecture we should have use. He suggested
model view controller would be a good fit becuase we could switch easily
from the commandline to GUI easily.

Alec reminded the team that that we should not take a niave approach and
keep our design as open as possible so we can fully utalize the server and
client relationship.

Alec asked us if we have any timeline or milstones for the near future
that we could work towards.
Shuan asked us to post on discord on sunday a write up / proposal of each architchure
so we have something to compare to when deciding on an architechure.

Jenna asked us to weight the pros and cons of each architechure and get
a result with a weighted decision matrix.

Paul had asked us how we would research each topic, and Shaun said we 
could talk about that in our routine work session. Chen suggested we
put a text file of our finding on the git hub.

Alec told the team they should consider good ADT etiquettes when deciding methods and preconditions.

Jenna reminded the team that when writing the report for design, dont include and code, and make sure
to describe tasks in great detail.

Alec told the team that we need to work while keeping in mind a 'change' would
occur sometime throughout the project that we must attend to.

Shaun suggested that everyone should read over the ASPECT J notes posted on the
class websites becuase it will play a large part of the project in the future.

Meeting concluded at 1:50
