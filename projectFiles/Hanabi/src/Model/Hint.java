package Model;

public class Hint {

    private char suit;
    private int rank;
    private static final char[] validSuits = new char[]{'b', 'g', 'm', 'r', 'w', 'y'};
    private static final int[] validRanks = new int[]{1, 2, 3, 4, 5};


    /**
     * Constructs a hint class with a suit and rank
     * @param suit suit in the hint as a charactor
     * @param rank rank in the hint as an integer
     */
    public Hint (char suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * Set the suit and rank field for the hint
     * @param suit suit in the hint as a character
     * @param rank rank in the hint as an integer
     */
    public void setHint (char suit,  int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * Obtain the suit in the hint
     * @return the suit as a character
     */
    public char getSuit () {
        return this.suit;
    }

    /**
     * Obtain the rank in the hint
     * @return the rank as an integer
     */
    public int getRank () {
        return this.rank;
    }

    /**
     * check if the hint given is valid in terms
     * @param suit suit of the hint
     * @param rank rank of the hint
     * @return boolean value indicating if the hint is valid
     */
    private boolean checkHint(char suit, int rank){
        boolean result = false;

        //Check for a valid rank
        for (int r : this.validRanks){
            result |= (rank == r);
        }
        if (!result){
            return result;
        }

        //Check for a valid suit
        result = false;
        for (char s : this.validSuits){
            result |= (suit == s);
        }
        return result;
    }

    public static void main (String [] args){

        Hint testHint = new Hint('g', 5);

        System.out.println("Case 1");
        System.out.println("The suit is: " + testHint.getSuit());
        System.out.println("The rank is: " + testHint.getRank());
        System.out.println("Is this a valid hint?: " + testHint.checkHint(testHint.getSuit(), testHint.getRank()));

        System.out.println("");

        testHint.setHint('z', 7);
        System.out.println("Case 2");
        System.out.println("The suit is: " + testHint.getSuit());
        System.out.println("The rank is: " + testHint.getRank());
        System.out.println("Is this a valid hint?: " + testHint.checkHint(testHint.getSuit(), testHint.getRank()));
    }
}
