Worklog for Feb 27, 2019
met between 1600-1800

Went over, for those who were not present at the work session over the break,
the finite state diagram. 

We also determined that we would go ahead with a gui interface instead of a
command line as we didn't think we would have enough time to get both done.
This switch of interfaces led to use re-working how the controller in the model
would get information.

We also divided up who would write which CRC cards and have them ready for
friday so that a UML Diagram can be constrcuted over the weekend. As well, the
design document has been started, though it will still need to be convereted to
latex, and some information has been included in it like a summary of the
finite state diagram for the event transition model we plan to use as the
controller for the MVC. 

we did some rought mock ups of the game board layout for the new GUI from 2-5
players. The 5 player model has already has a .xml mockup which was done by
Faiq. 
