Mar 14, 2019 Minute

Jenna was asking how we think we did on the test document and we were confident 
that we did well, especially comparing to the requirement document. We believe
that our test document is well-structured and well reviewed by each member. We
also did a brief feedback on the design document and were happy about the overall
improvement we have for the design document. One issue we encountered during the 
editing of all the documents is the compiling error with the latex files. This is
normal because latex documents are difficult to set up in general. Up next, we are
having our pair programming report due, and we need to make plans for the programming
tasks. We will decide on the combinations of pair programmers in the upcoming work 
sessions. We are planning to have 2-to-4-hour sessions for each pair programming 
session. We have to make notes of people who work hard but do not have to commit
onto git as often as other team members. We also need to look into AspectJ for the 
upcoming programming tasks. Shuan also mentioned that the network class needs some 
modifications as the current design will not work properlly with the server. In addition,
the time of when the server will be available is still uncertain. We plan to start setting
up the project files before Monday.Then we talked about the point of the change in the 
requirement might reflect on the robustness of the architecture. 
