package Controller;


import Model.Model;
import Model.Game;
import Network.Command;
import Network.Message;
import Startup.Client;
import View.View;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CreateGameController implements Initializable {

    @FXML
    private TextField nsidField;
    @FXML
    private TextField keyField;
    @FXML
    private Slider totalPlayersSlider;
    @FXML
    private Slider timeoutSlider;
    @FXML
    private ComboBox<String> rainbowBox;
    @FXML
    private CheckBox forceBox;
    @FXML
    private Button submitButton;

    private Model model;

    public CreateGameController (Model model) {
        this.model=model;
    }

    @Override
    public void initialize (URL url, ResourceBundle resourceBundle) {

        nsidField.setText(model.fetchNSID());
        rainbowBox.setValue("None");
        rainbowBox.getItems().addAll(
                "None",
                "Wild",
                "Rainbow"
        );

        submitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                String nsid = nsidField.getText();
                String key = keyField.getText();
                int totalPlayers = (int)totalPlayersSlider.getValue();
                int timeout = (int)timeoutSlider.getValue();
                String rainbow = rainbowBox.getValue();
                boolean force = forceBox.isSelected(); // for shauns message class

                if (!model.isValidNSID(nsid)) {
                    View.showAlert(Alert.AlertType.ERROR, "Invalid NSID",
                            "You must enter in a valid NSID!");
                }else{
                    model.setNSID(nsid);
                    if(key.length() != 32) {
                        View.showAlert(Alert.AlertType.ERROR, "Invalid Key length",
                                "Key should be 32 characters in length!");
                    } else {
                        toggleDisable();

                        model.setSecretKey(key);
                        Command aCommand = new Command("create", nsid, totalPlayers,
                        timeout, rainbow.toLowerCase(), force);

                        aCommand.send(Client.getNetwork().getOutStream(), key);

                        Message message = new Message();
                        message.recv(Client.getNetwork().getInStream());

                        String reply = message.getPayload().get("reply").toString();
                        if(reply.equals("invalid")) {
                            View.showAlert(Alert.AlertType.WARNING, "Invalid",
                                    "Invalid NSID or Token entered!");
                            Client.getNetwork().restart();
                            toggleDisable();
                        } else if (reply.equals("extant")) {

                            View.showAlert(Alert.AlertType.INFORMATION,
                                    "Game Extant",
                                    "A game already exists for this account!");
                            Game game = new Game(message.getPayload().get("game-id").toString(),
                                    message.getPayload().get("token").toString());
                            model.setGame(game);
                            model.setNSID(nsid);
                            model.setSecretKey(key);

                            Client.getNetwork().restart();

                            Stage stage = new Stage();

                            Object controller = Controller.getController("JoinGameController");
                            Parent parent = View.loadXML("JoinGame", controller);

                            stage.setScene(new Scene(parent));
                            stage.setTitle("Join Game");
                            stage.setResizable(false);
                            stage.centerOnScreen();
                            stage.show();

                            ((Node)mouseEvent.getSource()).getScene().getWindow().hide(); // close this window

                        } else if (reply.equals("created")) {

                            View.showAlert(Alert.AlertType.CONFIRMATION,
                                    "Game Created",
                                    "Your game has been successfully created!");
                            Game game = new Game(totalPlayers, timeout,
                                    message.getPayload().get("game-id").toString(),
                                    message.getPayload().get("token").toString());
                            model.setGame(game);
                            model.setRainbow(rainbow);
                            model.setNSID(nsid);
                            model.setSecretKey(key);

                            Stage stage = new Stage();

                            Object controller = Controller.getController("LobbyController");
                            Parent parent = View.loadXML("Lobby", controller);

                            stage.setScene(new Scene(parent));
                            stage.setTitle("Game Lobby");
                            stage.setResizable(false);
                            stage.centerOnScreen();

                            stage.show();

                            ((Node)mouseEvent.getSource()).getScene().getWindow().hide(); // close this window

                        }

                    }

                }

            }

        });
    }
    /**
     * Toggle between disable and enabled state of the various components,
     * to prevent user from submitting twice to the server
     */
    private void toggleDisable() {
        nsidField.setDisable(!nsidField.isDisabled());
        keyField.setDisable(!keyField.isDisabled());
        totalPlayersSlider.setDisable(!totalPlayersSlider.isDisabled());
        timeoutSlider.setDisable(!timeoutSlider.isDisabled());
        rainbowBox.setDisable(!rainbowBox.isDisabled());
        forceBox.setDisable(!forceBox.isDisabled());
        submitButton.setDisable(!submitButton.isDisabled());
    }
}
