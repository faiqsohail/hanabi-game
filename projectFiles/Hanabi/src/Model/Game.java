package Model;

import com.google.gson.JsonArray;
import org.json.simple.JSONArray;

public class Game {

    private int numberOfPlayers;
    private int timeOutPeriod;
    private int playerTurn;
    private String gameID;
    private String gameToken;

    private JSONArray init_hand;

    /**
     * Initialize the game class with only game id and game token,
     * to be used temporarily to pass information from the create game controller to the join game controller
     *  @param gameID set the ID of the game
     *  @param gameToken set the token for the game
     */
    public Game (String gameID, String gameToken) {
        this.gameID = gameID;
        this.gameToken = gameToken;
        this.playerTurn = 0;
    }


    /**
     * Initialize the game class
     * @param numberOfPlayers set the number of players in the game
     * @param timeOutPeriod set the time out period of the game
     * @param gameID set the ID of the game
     * @param gameToken set the token for the game
     */
    public Game (int numberOfPlayers, int timeOutPeriod, String gameID, String gameToken) {
        this(gameID, gameToken);
        this.numberOfPlayers = numberOfPlayers;
        this.timeOutPeriod = timeOutPeriod;
    }


    /**
     * set the number of players
     * @param number integer number
     */
    public void setNumberOfPlayers (int number) {
        if (number > 5) {
            this.numberOfPlayers = 5;
        } else {
            this.numberOfPlayers = number;
        }
    }

    /**
     * access the number of players
     * @return number of players as an integer
     */
    public int getNumberOfPlayers () {
        return this.numberOfPlayers;
    }

    /**
     * Set the time period for the game
     * @param timeOutPeriod the timeOutPeriod
     */
    public void setTimeoutPeriod (int timeOutPeriod) {
        this.timeOutPeriod = timeOutPeriod;
    }

    /**
     * access the time period of the game
     * @return the period of the game as an integer
     */
    public int getTimeOutPeriod () {
        return this.timeOutPeriod;
    }

    /**
     * set the ID fir the game
     * @param gameID ID for the game
     */
    public void setGameID (String gameID) {
        this.gameID = gameID;
    }

    /**
     * access the ID of the game
     * @return the ID of the game as a string
     */
    public String getGameID () {
        return this.gameID;
    }

    /**
     * set the game token
     * @param token the token to be set for the game
     */
    public void setGameToken (String token) {
        this.gameToken = token;
    }

    /**
     * access the hame token
     * @return the game token as a string
     */
    public String getGameToken () {
        return this.gameToken;

    }

    /**
     * progress the player's turn based on the current turn
     */
    public void progressPlayerTurn () {
        if (this.playerTurn >= this.numberOfPlayers - 1){
            this.playerTurn = 0;
        } else {
            this.playerTurn += 1;
        }
    }

    /**
     * obtain the initial hand
     * @return the initial hand as a JSONArray
     */
    public JSONArray getInit_hand() {
        return init_hand;
    }

    /**
     * set the initial hand
     * @param init_hand the initial hand as a JSONArray
     */
    public void setInit_hand(JSONArray init_hand) {
        this.init_hand = init_hand;
    }

    /**
     * Access the turn of the current player
     * @return the turn of the current player as an integer
     */
    public int getPlayerTurn () {
        return this.playerTurn;
    }

    public static void main (String [] args) {
        Game testGame = new Game(5, 60, "testGame", "alibaba");

        //Test the getter methods
        if (testGame.getNumberOfPlayers() != 5) {
            System.out.println("Error with getNumbeOfPlayers");
        }
        if (testGame.getTimeOutPeriod() != 60) {
            System.out.println("Error with getTimeOutPeriod");
        }
        if (testGame.getPlayerTurn() != 0) {
            System.out.println("Error with getPlayerTurn");
        }
        if (!testGame.getGameToken().equals("alibaba")) {
            System.out.println("Error with getGameToken");
        }
        if (!testGame.getGameID().equals("testGame")) {
            System.out.println("Error with getGameToken");
        }

        //Test the setter methods as well as the player progress method
        testGame.setNumberOfPlayers(3);
        if (testGame.getNumberOfPlayers() != 3) {
            System.out.println("Error with setNumbeOfPlayers");
        }

        testGame.setTimeoutPeriod(15);
        if (testGame.getTimeOutPeriod() != 15) {
            System.out.println("Error with setTimeOutPeriod");
        }

        testGame.progressPlayerTurn();
        if (testGame.getPlayerTurn() != 1) {
            System.out.println("Error with getPlayerTurn");
        }

        testGame.setGameToken("computer science");
        if (!testGame.getGameToken().equals("computer science")) {
            System.out.println("Error with setGameToken");
        }

        testGame.setGameID("e2 game");
        if (!testGame.getGameID().equals("e2 game")) {
            System.out.println("Error with setGameToken");
        }

        // test if the player turn progress back to 0 when it is supposed to
        testGame.progressPlayerTurn();
        testGame.progressPlayerTurn();

        if (testGame.getPlayerTurn() != 0) {
            System.out.println("Player turn does not got back to 0");
        }

    }

}
