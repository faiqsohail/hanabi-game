package View;

import Model.Card;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class PlayerCardView extends ImageView {

    private Card card;

    /**
     * Create a new PlayerCardView to represent a card visually onto the screen
     * @param image Image : An image that represents a Hanabi Card
     * @param card Card : A Card object that relates to the Image being displayed
     */
    public PlayerCardView (Image image, Card card) {
        super(image);
        this.card = card;
    }

    /**
     * Get the card representative of the PlayerCardView
     * @return Card
     */
    public Card getCard () {
        return card;
    }
}
