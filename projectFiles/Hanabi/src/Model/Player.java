package Model;

public class Player {

    //The player's hand
    protected Hand hand;
    protected final int playerId;

    /**
     * Construct a new player with the given hand and id
     * @param hand The hand of the player
     * @param playerId The player's Id
     */
    public Player (Hand hand, int playerId) {
        this.playerId = playerId;
        this.hand = hand;
    }

    /**
     * Get the hand of the player
     * @return The hand of the player
     */
    public Hand getHand () {
        return this.hand;
    }

    /**
     * Get the player's id
     * @return The player's id
     */
    public int getPlayerId() { return this.playerId; }

    /**
     * Give this player a card
     * @param position The position in hand the card will go
     * @param card The card to receive
     */
    public void receiveCard(int position, Card card){
        this.hand.receiveCard(position, card);
    }

    /**
     * Create a readable string representation of the player
     * @return A string representation of the player
     */
    public String toString () {
        String information = "Player " + this.playerId + " has these cards: \n" + this.hand.toString();

        return information;
    }

    public static void main (String [] args) {

        Hand testHand = new Hand(new String [] {"b1", "w1", "y1", "g2", "r5"}, 5);
        Player testPlayer = new Player(testHand, 1);

        //Check toString and that the constructor worked as intended
        System.out.println(testPlayer.toString());

        if (testPlayer.getHand() != testHand){
            System.out.println("Error, get hand does not return the correct hand");
        }

        if (testPlayer.getPlayerId() != 1){
            System.out.println("Error, get playerId does not return the correct playerId");
        }

        testPlayer.receiveCard(1, new Card("r1"));
        if (!testPlayer.getHand().getCard(1).toString().equals("r1")){
            System.out.println("Error, the player did not properly receive a card");
        }
    }

}
