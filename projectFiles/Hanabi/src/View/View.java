package View;

import Controller.Controller;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;

public class View {

    private Stage stage;
    private Controller controller;
    private static final String XMLPATH = "FXML/";

    public View (Stage stage, Controller controller) {
        this.stage = stage;
        this.stage.setTitle("Hanabi");
        this.stage.setResizable(false);
        this.stage.centerOnScreen();

        this.controller = controller;
        this.showWindow(this.initWindow());
    }

    /**
     * The initial Scene shown when the user first launches the application
     *
     * @return the Scene
     */
    public Scene initWindow () {
        Parent root = loadXML("InitialWindow",
                this.controller.getController("InitialWindowController"));
        return new Scene(root);
    }

    /**
     * Gets the FXML file as a InputStream given the name of the file
     *
     * @param fileName - String : name of the file to load without extension
     * @return InputStream
     */
    public static InputStream getFXML (String fileName) {
        return View.class.getResourceAsStream(XMLPATH + fileName + ".fxml");
    }

    /**
     * Loads an FXML file given the name of the file to load and the JavaFX controller
     * that the given FXML file will use
     * @param fileName - String : name of the file to load without extension
     * @param controller - Object : A valid JavaFX controller that implements initializable.
     * @return Parent
     */
    public static Parent loadXML (String fileName, Object controller)  {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setController(controller);
        try {
            return fxmlLoader.load(getFXML(fileName));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    /**
     * Creates the Game Window for the Hanabi Game
     *
     * @param numPlayers - int : number of players in the game (2 - 5)
     * @return Scene : Scene object loaded with the appropriate FXML file
     */
    public static Scene createWindow (int numPlayers) {
        if ((numPlayers > 5) || (numPlayers < 2)) {
            throw new RuntimeException("A Hanabi Game must consist of 2 - 5 players");
        }

        Object controller = Controller.getController("GameController");
        Parent root = loadXML(Integer.toString(numPlayers), controller);
        return new Scene(root);
    }

    /**
     * Shows a window onto the screen
     *
     * @param scene - Scene : Scene to show
     */
    public void showWindow (Scene scene) {
        this.stage.setScene(scene);
        this.stage.show();
    }

    /**
     * Shows a alert onto the screen
     *
     * @param alertType    Alert.AlertType
     * @param header       - String : Heading of the alert
     * @param alertContent - String : Content of the alert
     */
    public static void showAlert (Alert.AlertType alertType, String header, String alertContent) {
        Alert alert = new Alert(alertType, alertContent, ButtonType.OK);
        Stage dialogStage = (Stage) alert.getDialogPane().getScene().getWindow();
        dialogStage.setAlwaysOnTop(true);
        alert.setHeaderText(header);
        alert.show();
    }
}
