package Network;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.net.*;
import java.io.*;


public class Network {

    private String hostName;
    private int port;

    private BufferedReader inStream;
    private PrintStream outStream;
    private Socket connection;

    /**
     * The constructor
     */
    public Network () {
        this.hostName = "gpu2.usask.ca";
        this.port = 10219;

        try {
            connection = new Socket (hostName, port);  // setting up the socket connection
            inStream = new BufferedReader (new InputStreamReader(connection.getInputStream()));
            outStream = new PrintStream (connection.getOutputStream ()); // setting up output

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * the constructor taking in a port
     * @param port the port of connection
     */
    public Network (int port) {

        this.hostName = "gpu2.usask.ca";
        this.port = port;

        try {
            connection = new Socket(hostName, port);  // setting up the socket connection
            BufferedReader inStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            outStream = new PrintStream  (connection.getOutputStream ()); // setting up output

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * closes the connection
     */
    public void closeCon () {

        try{
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * obtain the in stream of the bufferrRader
     * @return the in stream of the bufferReader
     */
    public BufferedReader getInStream () {
        return inStream;
    }

    /**
     * obtain the out stream of the bufferReader
     * @return the out stream of the bufferReader
     */
    public PrintStream  getOutStream () {
        return outStream;
    }


    /**
     * Restart the connection to the server
     */
    public void restart () {
        this.hostName = "gpu2.usask.ca";
        this.port = 10219;

        closeCon();
        try {
            inStream.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        outStream.close();
        try {
            connection = new Socket (hostName, port);  // setting up the socket connection
            inStream = new BufferedReader (new InputStreamReader(connection.getInputStream()));
            outStream = new PrintStream (connection.getOutputStream ()); // setting up output

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /*
       Tried Testing the network on 3/20/2019
       Couldnt get connection to function properly
       Server may not be open yet - will need further testing
     */

    public static void main (String[] argv) {
        Network testNet = new Network();


        testNet.getOutStream().println("something");
        try {
            while (true)
                System.out.print((char) testNet.getInStream().read());
        } catch (IOException e){}


        System.out.println("DONE");
        testNet.closeCon();

    }

}
