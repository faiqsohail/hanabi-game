package Controller;

import Model.Model;
import Model.Game;
import Network.Command;
import Network.Message;
import Startup.Client;
import View.View;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class JoinGameController implements Initializable {

    @FXML
    private TextField nsidField;
    @FXML
    private TextField keyField;
    @FXML
    private TextField gameField;
    @FXML
    private TextField tokenField;
    @FXML
    private Button submitButton;

    private Model model;

    public JoinGameController (Model model) {
        this.model = model;
    }

    @Override
    public void initialize (URL url, ResourceBundle resourceBundle) {

        /* Pre-fill any information we already know */

        if(model.getNSID() != null) {
            nsidField.setText(model.getNSID());
        } else {
            nsidField.setText(model.fetchNSID());
        }

        keyField.setText(model.getSecretKey());

        if(model.getGame() != null) {
            Game game = model.getGame();
            gameField.setText(game.getGameID());
            tokenField.setText(game.getGameToken());
        }


        /* Set the button listener */
        submitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle (MouseEvent mouseEvent) {
                String nsid = nsidField.getText();
                String key = keyField.getText();
                String gameId = gameField.getText();
                String gameToken = tokenField.getText();

                if (!model.isValidNSID(nsid)) {
                    View.showAlert(Alert.AlertType.ERROR, "Invalid NSID", "You must enter in a valid NSID!");
                } else {
                    model.setNSID(nsid);
                    if (key.length() != 32) {
                        View.showAlert(Alert.AlertType.ERROR, "Invalid Key length", "Key should be 32 characters in length!");
                    } else {
                        Client.getNetwork().restart();
                        try {
                            toggleDisable();
                            model.setSecretKey(key);
                            Command aCommand = new Command("join", nsid, Integer.parseInt(gameId), gameToken);
                            aCommand.send(Client.getNetwork().getOutStream(), key);
                            Message message = new Message();
                            message.recv(Client.getNetwork().getInStream());


                            while (message.getPayload().containsKey("notice") ){
                                    message.recv(Client.getNetwork().getInStream());
                            }

                            String reply = message.getPayload().get("reply").toString();
                            if(reply.equals("invalid")) {
                                View.showAlert(Alert.AlertType.WARNING, "Invalid",
                                        "Invalid Information Entered");
                                Client.getNetwork().restart();
                                toggleDisable();

                            } else if (reply.equals("game full")) {
                                View.showAlert(Alert.AlertType.ERROR, "Game Full",
                                        "The game your trying to join is full!");
                                Client.getNetwork().restart();
                                toggleDisable();
                            } else if (reply.equals("joined")) {

                                Game game = new Game(gameId, gameToken);
                                game.setTimeoutPeriod(Integer.parseInt(message.getPayload().get("timeout").toString()));
                                model.setGame(game);

                                String rainbow = message.getPayload().get("rainbow").toString();
                                model.setRainbow(rainbow.substring(0, 1).toUpperCase() + rainbow.substring(1));

                                Stage stage = new Stage();

                                Object controller = Controller.getController("LobbyController");
                                Parent parent = View.loadXML("Lobby", controller);

                                stage.setScene(new Scene(parent));
                                stage.setTitle("Game Lobby");
                                stage.setResizable(false);
                                stage.centerOnScreen();

                                stage.show();

                                ((Node)mouseEvent.getSource()).getScene().getWindow().hide(); // close this window
                            }

                        } catch (NumberFormatException e) {
                            toggleDisable();
                            View.showAlert(Alert.AlertType.ERROR, "Invalid Game ID", "You must enter in a valid numeric Game ID!");
                        }
                    }
                }
            }
        });
    }

    /**
     * Toggle between disable and enabled state of the various components,
     * to prevent user from submitting twice to the server
     */
    private void toggleDisable() {
        nsidField.setDisable(!nsidField.isDisabled());
        keyField.setDisable(!keyField.isDisabled());
        gameField.setDisable(!gameField.isDisabled());
        tokenField.setDisable(!tokenField.isDisabled());
        submitButton.setDisable(!submitButton.isDisabled());
    }

}

