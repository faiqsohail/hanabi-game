package Network;


import org.json.simple.JSONObject;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Command extends Message {

    protected Map obj;

    /**
     * This constructor is going to create the create game message
     * @param cmd the cmd
     * @param nsid the nsid
     * @param players the players
     * @param timeout the timeout
     * @param rainbow the rainbow mode string
     * @param force the force boolean
     */
    public Command (String cmd, String nsid, int players,
                               int timeout, String rainbow, boolean force) {

        obj = new LinkedHashMap();

        obj.put("cmd", cmd);
        obj.put("nsid", nsid);
        obj.put("players", players);
        obj.put("timeout", timeout);
        obj.put("force", force);
        obj.put("rainbow", rainbow);
        obj.put("timestamp","");
        obj.put("md5hash","");

        payload = new JSONObject(obj);

    }

    /**
     * This constructor set up the join game message.
      * @param cmd the cmd
     * @param nsid the nsid
     * @param gameID the gameID
     * @param token the token
     */
    public Command (String cmd, String nsid, int gameID, String token) {

        obj = new LinkedHashMap();

        obj.put("cmd", cmd);
        obj.put("nsid", nsid);
        obj.put("game-id", gameID);
        obj.put("token", token);
        obj.put("timestamp","");
        obj.put("md5hash","");

        payload = new JSONObject(obj);
    }

    /**
     * Sets the MD5 hash for signing a command including secret preloading
     * @param key the key
     */
    private void setMD5Hash (String key) {

        payload.put("md5hash", key);
        String hash = computeHash(toString());
        payload.put("md5hash", hash);
    }



    /**
     * This function takes a message and obtains the hash value
     * assumes that the message has been pre-loaded with the approriate
     * secret
     * @param msg the message
     * @return the string
     */
    private String computeHash (String msg) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(msg.getBytes());

            return (new BigInteger(1, md.digest())).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return ("MD5 is an unknown hashing algorithm");
        }
    }

    /**
     * Very similar to the parrent's send but here we need the secret hash
     * so that we can digitaly sign this message.
     * @param out the out stream
     * @param hash the hash string
     */
    public void send (PrintStream out, String hash) {

        setTimestamp();
        setMD5Hash(hash);

        out.print(toString());
        out.flush();
    }

    /**
     * Overides the Message toString because we need to keep order
     * of the JSON elements.
     * @return the string information of the class
     */
    @Override
    public String toString () {


        obj.put("md5hash", payload.get("md5hash"));
        obj.put("timestamp", payload.get("timestamp"));

        return payload.toJSONString(obj);
    }

    public static void main (String argv[]) {

        // Test creating Create Message
        Command testCreate = new Command ("create", "swl567", 5,
                                        60, "none", false);

        // Test creating Join Message
        Command testJoin = new Command ("join", "swl567",
                                        24, "Hello There");


        //test send (Just making sure that the time and ms5hash are no longer empty)
        try{
            FileOutputStream fileOut = new FileOutputStream("TCPConnection2");
            PrintStream dataOut = new PrintStream(fileOut);

            testCreate.send(dataOut,"8201f5c858020aebbe8888c5ae4dc13f");
            testJoin.send(dataOut,"8201f5c858020aebbe8888c5ae4dc13f");

            //testing create
            if(testCreate.getPayload().get("timestamp").equals("")){
                System.err.println("Error setting Create Timestamp");
            }
            if(testCreate.getPayload().get("md5hash").equals("")){
                System.err.println("Error setting Create md5hash");
            }

            //testing join
            if(testJoin.getPayload().get("timestamp").equals("")){
                System.err.println("Error setting Join Timestamp");
            }
            if(testJoin.getPayload().get("md5hash").equals("")){
                System.out.println("Error setting Join md5hash");
            }

        } catch (IOException e){
            System.err.println(e);
        }

        Network testCon = new Network();

        //testing invalid response from server
        testCreate.send(testCon.getOutStream(),"8201f5c858020aebbe8888c5se4dc13f");
        Message createRecv = new Message(testCon.getInStream());
        if (!createRecv.getPayload().get("reply").equals("invalid")) {
            System.out.println("Server didn't respond with invalid");
            System.out.println(createRecv.toString());
        }
        if (createRecv.getPayload().get("reply").toString().equals("invalid")) {
            testCon.restart();
        }

        //testing valid response from server
        Command create = new Command("create", "swl567", 3,
                120, "none", true);

        create.send(testCon.getOutStream(),"8201f5c858020aebbe8888c5ae4dc13f");

        //testing for game creation
        createRecv.recv(testCon.getInStream());

        if (!createRecv.getPayload().get("reply").equals("created")) {
            System.out.print("Game create failed");
        }

        int gameId = Integer.parseInt(createRecv.getPayload().get("game-id").toString());
        String gameToken = createRecv.getPayload().get("token").toString();

        //testing for game join
        createRecv.recv(testCon.getInStream());

        if (!createRecv.getPayload().get("reply").equals("joined")) {
            System.out.print("Game join failed");
        }

        //testing Join game
        Network joinCon = new Network();

        Command join = new Command ("join", "mek838", gameId, gameToken);

        join.send(joinCon.getOutStream(),"5899f88f3fcab344ae136d9eda597538");

        //need to read twice once for the reply and again for the notice
        Message joinRecv = new Message(joinCon.getInStream());

        if (joinRecv.getPayload().containsKey("notice")) {
            if (!joinRecv.getPayload().get("notice").equals("player joined")) {
                System.out.println("Error notice joining game");
            }
        }
        else if (joinRecv.getPayload().containsKey("reply")) {
            if (!joinRecv.getPayload().get("reply").equals("joined")) {
                System.out.println("Error reply joining game");
            }
        }
        else {
            System.out.println("Recv unknown response");
        }


        joinRecv.recv(joinCon.getInStream());

        if (joinRecv.getPayload().containsKey("notice")) {
            if (!joinRecv.getPayload().get("notice").equals("player joined")) {
                System.out.println("Error notice joining game");
            }
        }
        else if (joinRecv.getPayload().containsKey("reply")) {
            if (!joinRecv.getPayload().get("reply").equals("joined")) {
                System.out.println("Error reply joining game");
            }
        }
        else {
            System.out.println("Recv unknown response");
        }
    }

}
